create database Fruits_Shop
GO
use Fruits_Shop
GO
CREATE TABLE [dbo].[ChiTietBan](
	[ID_HoaDonBan] [int] NOT NULL,
	[ID_SanPham] [int] NOT NULL,
	[SoLuongBan(KG)] [int] NULL,
	[GiaBan(VND)] [int] NULL,
 CONSTRAINT [PK_ChiTietBan] PRIMARY KEY CLUSTERED 
(
	[ID_HoaDonBan] ASC,
	[ID_SanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChiTietMua]    Script Date: 2021-11-07 7:07:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietMua](
	[ID_HoaDonMua] [int] NOT NULL,
	[ID_SanPham] [int] NOT NULL,
	[SoLuongMua(KG)] [int] NULL,
	[GiaMua(VND)] [int] NULL,
 CONSTRAINT [PK_ChiTietMua] PRIMARY KEY CLUSTERED 
(
	[ID_HoaDonMua] ASC,
	[ID_SanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GioHang]    Script Date: 2021-11-07 7:07:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GioHang](
	[ID_TaiKhoan] [int] NOT NULL,
	[ID_SanPham] [int] NOT NULL,
	[SoLuongTrongGioHang(KG)] [int] NULL,
 CONSTRAINT [PK_GioHang] PRIMARY KEY CLUSTERED 
(
	[ID_TaiKhoan] ASC,
	[ID_SanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HoaDonBan]    Script Date: 2021-11-07 7:07:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDonBan](
	[ID_HoaDonBan] [int] IDENTITY(1,1) NOT NULL,
	[ID_TaiKhoanAdmin] [int] NULL,
	[ID_TaiKhoanKH] [int] NULL,
	[NgayLapHoaDon] [date] NULL,
	[TrangThai] [bit] NULL,
 CONSTRAINT [PK_HoaDonBan] PRIMARY KEY CLUSTERED 
(
	[ID_HoaDonBan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HoaDonMua]    Script Date: 2021-11-07 7:07:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDonMua](
	[ID_HoaDonMua] [int] IDENTITY(1,1) NOT NULL,
	[ID_TaiKhoan] [int] NULL,
	[ID_NhaPhanPhoi] [int] NULL,
	[NgayLapHoaDon] [date] NULL,
 CONSTRAINT [PK_HoaDonMua] PRIMARY KEY CLUSTERED 
(
	[ID_HoaDonMua] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KhoHang]    Script Date: 2021-11-07 7:07:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhoHang](
	[ID_SanPham] [int] NOT NULL,
	[SoLuongTonKho(KG)] [int] NULL,
	[GiaBan(VND)] [int] NULL,
 CONSTRAINT [PK_KhoHang] PRIMARY KEY CLUSTERED 
(
	[ID_SanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NhaPhanPhoi]    Script Date: 2021-11-07 7:07:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhaPhanPhoi](
	[ID_NhaPhanPhoi] [int] IDENTITY(1,1) NOT NULL,
	[TenNhaPhanPhoi] [nvarchar](max) NOT NULL,
	[SDT] [nvarchar](max) NULL,
	[DiaChi] [nvarchar](max) NULL,
	[Deleted] [bit] NULL,
 CONSTRAINT [PK_NhaPhanPhoi] PRIMARY KEY CLUSTERED 
(
	[ID_NhaPhanPhoi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhanLoai]    Script Date: 2021-11-07 7:07:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhanLoai](
	[ID_PhanLoai] [int] IDENTITY(1,1) NOT NULL,
	[TenPhanLoai] [nvarchar](max) NULL,
 CONSTRAINT [PK_PhanLoai] PRIMARY KEY CLUSTERED 
(
	[ID_PhanLoai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SanPham]    Script Date: 2021-11-07 7:07:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SanPham](
	[ID_SanPham] [int] IDENTITY(1,1) NOT NULL,
	[TenSanPham] [nvarchar](max) NOT NULL,
	[AnhSanPham] [nvarchar](max) NULL,
	[ThongTinSanPham] [nvarchar](max) NULL,
	[ID_PhanLoai] [int] NULL,
	[Deleted] [bit] NULL,
 CONSTRAINT [PK_SanPham] PRIMARY KEY CLUSTERED 
(
	[ID_SanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ThongTinNguoiDung]    Script Date: 2021-11-07 7:07:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThongTinNguoiDung](
	[ID_TaiKhoan] [int] IDENTITY(1,1) NOT NULL,
	[TaiKhoan] [nvarchar](max) NOT NULL,
	[MatKhau] [nvarchar](max) NOT NULL,
	[Ten] [nvarchar](max) NULL,
	[NamSinh] [nvarchar](max) NULL,
	[SDT] [nvarchar](max) NULL,
	[DiaChi] [nvarchar](max) NULL,
	[isAdmin] [bit] NULL,
 CONSTRAINT [PK_TaiKhoan] PRIMARY KEY CLUSTERED 
(
	[ID_TaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (2, 8, 20, 26000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (3, 12, 2, 1100000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (4, 1, 2, 23000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (5, 5, 2, 8000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (5, 7, 3, 5000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (6, 9, 1, 8000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (7, 8, 3, 26000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (8, 10, 1, 52000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (8, 12, 2, 1100000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (8, 15, 2, 590000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (9, 5, 1, 8000)
INSERT [dbo].[ChiTietBan] ([ID_HoaDonBan], [ID_SanPham], [SoLuongBan(KG)], [GiaBan(VND)]) VALUES (10, 8, 2, 26000)
GO
INSERT [dbo].[ChiTietMua] ([ID_HoaDonMua], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)]) VALUES (1, 10, 32, 32000)
INSERT [dbo].[ChiTietMua] ([ID_HoaDonMua], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)]) VALUES (1, 11, 6, 66666)
INSERT [dbo].[ChiTietMua] ([ID_HoaDonMua], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)]) VALUES (1, 12, 3232, 213000)
INSERT [dbo].[ChiTietMua] ([ID_HoaDonMua], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)]) VALUES (2, 22, 4, 444444)
INSERT [dbo].[ChiTietMua] ([ID_HoaDonMua], [ID_SanPham], [SoLuongMua(KG)], [GiaMua(VND)]) VALUES (2, 23, 5, 55555)
GO
INSERT [dbo].[GioHang] ([ID_TaiKhoan], [ID_SanPham], [SoLuongTrongGioHang(KG)]) VALUES (3, 6, 1)
INSERT [dbo].[GioHang] ([ID_TaiKhoan], [ID_SanPham], [SoLuongTrongGioHang(KG)]) VALUES (3, 7, 1)
INSERT [dbo].[GioHang] ([ID_TaiKhoan], [ID_SanPham], [SoLuongTrongGioHang(KG)]) VALUES (4, 9, 300)
GO
SET IDENTITY_INSERT [dbo].[HoaDonBan] ON 

INSERT [dbo].[HoaDonBan] ([ID_HoaDonBan], [ID_TaiKhoanAdmin], [ID_TaiKhoanKH], [NgayLapHoaDon], [TrangThai]) VALUES (2, 1, 2, CAST(N'2021-11-05' AS Date), 1)
INSERT [dbo].[HoaDonBan] ([ID_HoaDonBan], [ID_TaiKhoanAdmin], [ID_TaiKhoanKH], [NgayLapHoaDon], [TrangThai]) VALUES (3, 3, 3, CAST(N'2021-11-05' AS Date), 1)
INSERT [dbo].[HoaDonBan] ([ID_HoaDonBan], [ID_TaiKhoanAdmin], [ID_TaiKhoanKH], [NgayLapHoaDon], [TrangThai]) VALUES (4, 3, 4, CAST(N'2021-11-06' AS Date), 1)
INSERT [dbo].[HoaDonBan] ([ID_HoaDonBan], [ID_TaiKhoanAdmin], [ID_TaiKhoanKH], [NgayLapHoaDon], [TrangThai]) VALUES (5, 3, 3, CAST(N'2021-11-06' AS Date), 1)
INSERT [dbo].[HoaDonBan] ([ID_HoaDonBan], [ID_TaiKhoanAdmin], [ID_TaiKhoanKH], [NgayLapHoaDon], [TrangThai]) VALUES (6, 3, 3, CAST(N'2021-11-07' AS Date), 1)
INSERT [dbo].[HoaDonBan] ([ID_HoaDonBan], [ID_TaiKhoanAdmin], [ID_TaiKhoanKH], [NgayLapHoaDon], [TrangThai]) VALUES (7, 3, 3, CAST(N'2021-11-07' AS Date), 1)
INSERT [dbo].[HoaDonBan] ([ID_HoaDonBan], [ID_TaiKhoanAdmin], [ID_TaiKhoanKH], [NgayLapHoaDon], [TrangThai]) VALUES (8, 3, 3, CAST(N'2021-11-07' AS Date), 1)
INSERT [dbo].[HoaDonBan] ([ID_HoaDonBan], [ID_TaiKhoanAdmin], [ID_TaiKhoanKH], [NgayLapHoaDon], [TrangThai]) VALUES (9, 3, 3, CAST(N'2021-11-07' AS Date), 1)
INSERT [dbo].[HoaDonBan] ([ID_HoaDonBan], [ID_TaiKhoanAdmin], [ID_TaiKhoanKH], [NgayLapHoaDon], [TrangThai]) VALUES (10, 3, 4, CAST(N'2021-11-07' AS Date), 1)
SET IDENTITY_INSERT [dbo].[HoaDonBan] OFF
GO
SET IDENTITY_INSERT [dbo].[HoaDonMua] ON 

INSERT [dbo].[HoaDonMua] ([ID_HoaDonMua], [ID_TaiKhoan], [ID_NhaPhanPhoi], [NgayLapHoaDon]) VALUES (1, 3, 6, CAST(N'2021-11-06' AS Date))
INSERT [dbo].[HoaDonMua] ([ID_HoaDonMua], [ID_TaiKhoan], [ID_NhaPhanPhoi], [NgayLapHoaDon]) VALUES (2, 3, 7, CAST(N'2021-11-07' AS Date))
SET IDENTITY_INSERT [dbo].[HoaDonMua] OFF
GO
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (1, 498, 23000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (2, 400, 150000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (3, 540, 55000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (4, 350, 145000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (5, 999, 8000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (6, 250, 22500)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (7, 300, 5000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (8, 175, 26000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (9, 370, 8000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (10, 449, 52000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (11, 135, 280000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (12, 148, 1100000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (15, 548, 590000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (16, 100, 85000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (17, 200, 65000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (18, 50, 555000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (19, 400, 25000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (20, 100, 1200000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (21, 10, 599000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (22, 104, 499000)
INSERT [dbo].[KhoHang] ([ID_SanPham], [SoLuongTonKho(KG)], [GiaBan(VND)]) VALUES (23, 25, 550000)
GO
SET IDENTITY_INSERT [dbo].[NhaPhanPhoi] ON 

INSERT [dbo].[NhaPhanPhoi] ([ID_NhaPhanPhoi], [TenNhaPhanPhoi], [SDT], [DiaChi], [Deleted]) VALUES (4, N'HomeFarm', N'12345678', N'HCM', 0)
INSERT [dbo].[NhaPhanPhoi] ([ID_NhaPhanPhoi], [TenNhaPhanPhoi], [SDT], [DiaChi], [Deleted]) VALUES (5, N'CevisOne', N'87654321', N'Hà Nội', 0)
INSERT [dbo].[NhaPhanPhoi] ([ID_NhaPhanPhoi], [TenNhaPhanPhoi], [SDT], [DiaChi], [Deleted]) VALUES (6, N'V-FOOD Việt Nam', N'135792345', N'Cần Thơ', 0)
INSERT [dbo].[NhaPhanPhoi] ([ID_NhaPhanPhoi], [TenNhaPhanPhoi], [SDT], [DiaChi], [Deleted]) VALUES (7, N'Tran Lien Hoan', N'09876543213', N'Binh Thạn', 1)
SET IDENTITY_INSERT [dbo].[NhaPhanPhoi] OFF
GO
SET IDENTITY_INSERT [dbo].[PhanLoai] ON 

INSERT [dbo].[PhanLoai] ([ID_PhanLoai], [TenPhanLoai]) VALUES (1, N'0-50k')
INSERT [dbo].[PhanLoai] ([ID_PhanLoai], [TenPhanLoai]) VALUES (2, N'50k-200k')
INSERT [dbo].[PhanLoai] ([ID_PhanLoai], [TenPhanLoai]) VALUES (3, N'200k-500k')
INSERT [dbo].[PhanLoai] ([ID_PhanLoai], [TenPhanLoai]) VALUES (4, N'500k-1000k')
INSERT [dbo].[PhanLoai] ([ID_PhanLoai], [TenPhanLoai]) VALUES (5, N'>1000k')
SET IDENTITY_INSERT [dbo].[PhanLoai] OFF
GO
SET IDENTITY_INSERT [dbo].[SanPham] ON 

INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (1, N'Mit', N'https://nongtraiblao.com/image/cache/catalog/products/mit-thai-da-xanhfarm-8616x211020043613-227-600x600.jpg', N'Quả mít chín có múi to, vỏ ngoài mỏng, cơm mít dầy và hạt rất nhỏ, mít chín có mùi rất thơm và ngọt. Ngoài ra, ăn mít còn rất có lợi cho sức khỏe, giúp cung cấp hàm lượng canxi cao, kali và các loại vitamin A, vitamin C… có lợi cho sức khỏe của bạn.', 1, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (2, N'Nho', N'http://product.hstatic.net/200000370273/product/0dd79373c7394324b698cf84611517db_2910e765f3a7449aa35f43382667d744_grande.jpg', N'Nho nổi tiếng với vị ngọt thanh khiết và giá cả hợp với xu hướng người tiêu dùng. Nho được nhiều người ưa chuộng như vậy là nhờ có các tính chất, chất lượng đặc thù, khác biệt so với các loại quả khác. Nho có vỏ dày, vị rôn rốt (hơi chua) và có hạt.', 2, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (3, N'Cam Xoan', N'https://chopho.com.vn/wp-content/uploads/2020/08/Cam-Xo%C3%A0n-600x600.jpg', N'Cam là một loại trái cây ngon và được nhiều người yêu thích. Cam thì có rất nhiều giống loại như là: cam sành, cam canh, cam khe mây Hà Tĩnh,... thì cam xoàn của miền tây là loại trái cây đặc sản được yêu thích nhất, bởi độ thơm ngon và ngọt nhất, kích thước vị giác.', 2, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (4, N'Kiwi', N'https://sieuthivivo.com/wp-content/uploads/2020/02/kiwi-xanh-1.jpg', N'Quả kiwi được mệnh danh là “siêu trái cây” bởi nó chứa hàm lượng lớn các dưỡng chất, vitamin. Các khoáng chất như Mg, Cu, K, Zn trong quả kiwi có vai trò rất lớn giữ trái tim luôn khỏe mạnh, và Ca, Cr, Cu giúp hệ xương cứng chắc, các khớp nối hoạt động trơn chu. Chất xơ phong phú trong kiwi hỗ trợ tiêu hóa rất tốt, ngăn ngừa táo bón, trĩ, ung thư ruột, đại tràng, đồng thời hỗ trợ giảm cân', 2, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (5, N'Thom', N'https://anhoafood.com/wp-content/uploads/2020/11/th%C6%A1m-m%E1%BA%ADt-kh%C3%B3m-m%E1%BA%ADt-d%E1%BB%A9a-m%E1%BA%ADt-h%E1%BB%AFu-c%C6%A1-%C4%90%C3%A0-L%E1%BA%A1t-anhoafood-3-600x600.jpg', N'Dứa có các tên gọi khác như là: khóm, thơm, khớm, gai hoặc huyền nương, tên khoa học Ananas comosus, là một loại quả nhiệt đới. Dứa là cây bản địa của Paraguay và miền nam Brasil . Quả dứa thường gọi thực ra là trục của bông hoa và các lá bắc mọng nước tụ hợp lại, còn quả thật là các ', 1, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (6, N'Thanh Long', N'https://tse1.mm.bing.net/th?id=OIP.CqYLWYcZ-u93EIlcnBVhLAAAAA&pid=Api&P=0&w=300&h=300', N'Thanh long là một loại quả mọc trên cây xương rồng Hylocereus, hay còn được gọi là nữ hoàng Honolulu – một loài cây có hoa chỉ nở vào ban đêm. Cây thanh long có nguồn gốc từ Trung Mỹ và miền nam Mexico. Hiện nay, quả thanh long được trồng chủ yếu ở các vùng nhiệt đới trên thế giới.', 1, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (7, N'Dua Hau', N'https://chopho.com.vn/wp-content/uploads/2020/08/D%C6%B0a-H%E1%BA%A5u-Kh%C3%B4ng-H%E1%BA%A1t-600x600.jpg', N'Dưa hấu có tên khoa học là Citrullus lanatus, là một loại thực vật thuộc họ bầu bí, vỏ cứng, chứa nhiều nước, có nguồn gốc từ miền nam Châu Phi. Dưa hấu được được nhiều người ưa chuộng bởi tính ngọt mát và nhiều nước, đồng thời còn giúp cung cấp nhiều vitamin và các nguyên tố vi lượng cho cơ thể.', 1, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (8, N'Chuoi', N'https://static.mymarketcdn.com/uploads/2020/02/105806_1-1.jpg', N'Chuối là tên gọi các loài cây thuộc chi Musa; quả của nó được coi là trái cây được ăn rộng rãi nhất. Những cây này có gốc từ vùng nhiệt đới ở Đông Nam Á và Úc. Ngày nay, nó được trồng khắp vùng nhiệt đới. Chuối được trồng ở ít nhất 107 quốc gia. Ở nhiều vùng trên thế giới và trong thương mại, "chuối" là từ thường được dùng để chỉ các loại quả chuối mềm và ngọt.', 1, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (9, N'Chom Chom', N'https://vinatt.com/vi/wp-content/uploads/2019/04/3-600x600-600x600.png', N'Thành phần hóa học cây chôm chôm: Hạt chôm chôm chứa 35-40% chất dầu béo đặc, có cấu trúc của hạt ca cao, có mùi dễ chịu, gồm phần lớn là arachidin, cùng với olein và stearin. Vỏ quả chứa tanin và một saponin độc. Vỏ cây và quả xanh có chứa tanin.', 1, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (10, N'Buoi', N'https://storage.googleapis.com/mm-online-bucket/ecommerce-website/uploads/media/105977.jpg', N'Thành phần của quả bưởi gồm chất như protein, chất béo, carbohydrate, chất xơ, hàm lượng vitamin C phong phú, vitamin B2, viatamin P, carotene, insulin… và nguyên tố vi lượng như canxi, kali, phốt pho, sắt… nên rất có lợi cho cơ thể.', 2, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (11, N'Cherry', N'https://salt.tikicdn.com/ts/product/81/93/ab/64cc893c3d385bf230c21f5d2ad6f4b7.jpg', N'hành phần dinh dưỡng chứa trong quả cherry có chứa nhiều chất chống oxy hóa, kali, chất xơ, đồng, sắt, canxi, magie, kẽm, phốt pho. Tất cả những dưỡng chất này đều là yếu tố quan trọng cho phụ nữ mang thai.', 3, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (12, N'Nho Mau Don', N'https://sieuthivivo.com/wp-content/uploads/2020/02/nho-mau-don-han-quoc-1.jpg', N'Chùm nho mẫu đơn trông mọng nước, có hình tròn đều, vị ngọt đậm đà, thoảng hương thơm nhẹ và có giá thành khá đắt. Vậy nho mẫu đơn là gì? Các loại nho mẫu đơn, cách chọn mua và bảo quản được lâu.', 5, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (15, N'Tao Sekai-ichi', N'http://familyfruits.com.vn/wp-content/uploads/2021/01/tao-sekai-ichi-nhat.jpg', N'Không thơm bằng táo Aomori nhưng màu sắc của Sekai Ichi khá bắt mắt và giòn. Vỏ táo căng bóng màu đỏ sậm, vị giòn ngọt và thơm đậm đà. Trong khi tất cả giống táo khác trên thế giới đều được áp dụng quy trình trồng, chăm sóc, thu hoạch... bằng máy móc thì táo Sekai Ichi lại hoàn toàn thủ công. Khi ra hoa, toàn bộ vườn táo được thụ phấn bằng tay. ', 4, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (16, N'Cacao', N'https://toplist.vn/images/800px/bot-ca-cao-nguyen-chat-duoc-tin-dung-nhat-hien-nay-434821.jpg', N'Cacao có nguồn gốc từ Trung Mỹ, có tên La Tinh là Theobroma cacao, có nghĩa là “thức ăn của các vị thần” – Food of the Gods. Chính hương vị thơm ngon & tác dụng tuyệt vời của cacao mà nó đã nhanh chóng trở thành thức uống được ưa chuộng nhất hiện nay', 2, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (17, N'Sầu Riêng', N'https://vietjet.asia/assets/uploads/2017/06/Di-may-bay-co-duoc-mang-sau-rieng-hay-khong.jpg', N'Một trái sầu riêng có từ 1 đến 6 múi, mỗi múi có từ 1 đên vài hạt tùy theo múi lớn nhỏ. Cơm sầu riêng bao quanh hột sầu riêng. Hạt sầu riêng có loại hạt lép, hạt không lép và hạt lép nữa, hạt sầu riêng có thể ăn được nếu được nướng, chiên hay luộc. Theo kinh nghiệm dân gian hạt sầu riêng có tác dụng trong việc chữa loét dạ dày và một số công dụng với sức khỏe con người.', 2, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (18, N'Phúc Bồn Tử', N'https://dacnguyen.vn/wp-content/uploads/2021/06/Qua-phuc-bon-tu-1.jpg', N'Cây Phúc bồn tử là cây nhỏ, kích thước nhỏ, mọc trườn, thân cành, cuống lá, cuống hoa có nhiều gai nhỏ. Lá cây mọc đơn, so le, có cuống dài, phiến lá chia thành 5 thùy không đều nhau. Gân lá hình chân vịt, mép có răng cưa không đều nhau, mặt trên phủ nhiều lông, mặt dưới phủ lông mềm màu xám.', 4, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (19, N'Xoài', N'https://lh3.googleusercontent.com/proxy/y7Q3ysIwdgmNQhuNHdW2KjBoX3R9e9B3KghTP-tCluDU6ltwtKwmVs35I8rw21qlP05Dm79oozMsiZ_3Ck5PFg4VXYCb2rQwuYjapwj3OHfZJTyzY8unREyV5yii-bzu7Dz_iQnI0ZeBRkJElPaJ9TsfuF_h5g', N'Xoài là cây ăn quả thân gỗ mọc rất khỏe, xoài có rất nhiều loại khác nhau nhưng nhìn chung à khi chín đều khoác trên mình chiếc áo màu xanh, quả bé chỉ bằng hai ngón tay, qua thời gian quả to lớn dần và chuyển sang màu vàng ươm, chín rộ trông rất hấp dẫn.', 1, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (20, N'Việt Quất', N'https://salt.tikicdn.com/ts/tmp/0e/cd/0c/851959e1cc8567d8928a76e9e6d84ec7.jpg', N'Việt quất hay còn được gọi với cái tên Blueberry (tên khoa học là Vaccinium myrtillus) thuộc họ Thạch Nam, sống thành từng bụi và loại cây lâu năm. - Việt quất có hoa màu trắng, hình vuông, còn trái thuộc dạng quả mọng, kết thành chùm, có màu đậm gần như đen với một chút ánh tía, thịt quả có màu đỏ hoặc ngả sang tím.', 5, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (21, N'Nho Xanh Không Hạt Mỹ', N'https://chopho.com.vn/wp-content/uploads/2020/07/1494810010_nho-xanh-khong-hat-australia-600x600.jpg', N'Nho bắt đầu được thu hoạch từ tháng 01 đến tháng 05 năm sau. Nho Xanh không hạt Mỹ phổ biến là: Thompson, Pristine, Autumn King quả dài thuân, màu xanh hổ phách, vị ngọt mát, rất giòn và đặc biệt là không có hạt. Nho xanh không hạt thường được ăn tươi, khô, ép nước, salad, nho kem…', 4, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (22, N'Nho Đen Khôgn Hạt Mỹ', N'https://product.hstatic.net/1000386986/product/img_product_4_16df04683a384db886b6dc9f9e8fb108_grande.jpg', N'Với hình dáng thuôn dài, vỏ mỏng màu đen sẫm hấp dẫn, không hạt, mọng nước, thịt cứng, giòn, có vị ngọt thanh mát, cuốn xanh, Nho đen không hạt Mỹ luôn là món trái cây tráng miệng hằng ngày cho gia đình bạn.', 3, 0)
INSERT [dbo].[SanPham] ([ID_SanPham], [TenSanPham], [AnhSanPham], [ThongTinSanPham], [ID_PhanLoai], [Deleted]) VALUES (23, N'Cherry Đỏ Úc', N'http://product.hstatic.net/1000396694/product/cherry_b228c2906d8d4b9a816f430e81dae73d_grande.jpg', N'Cherry đỏ Úc quả to, vị ngọt,  màu đỏ tươi, quả chắc. Cherry là mặt hàng được Khách hàng Việt Nam đặc biệt ưa chuộng, được coi là một đặc sản đắt tiền và thường dùng để làm quà biếu.', 4, 0)
SET IDENTITY_INSERT [dbo].[SanPham] OFF
GO
SET IDENTITY_INSERT [dbo].[ThongTinNguoiDung] ON 

INSERT [dbo].[ThongTinNguoiDung] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [isAdmin]) VALUES (1, N'admin', N'21232f297a57a5a743894a0e4a801fc3', N'Admin', N'2001', N'123456789', N'FPT Cần Thơ', 1)
INSERT [dbo].[ThongTinNguoiDung] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [isAdmin]) VALUES (2, N'ngocson1', N'25d55ad283aa400af464c76d713c07ad', N'Ngọc Sơn', N'2001', N'12345678', N'Kiên Giang', 0)
INSERT [dbo].[ThongTinNguoiDung] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [isAdmin]) VALUES (3, N'duyennpce', N'cab0da2f3257a99f8d2dbd7c1c109e18', N'Nguyen Phước Duyên', N'2001', N'0788751149', N'Đồng Tháp', 1)
INSERT [dbo].[ThongTinNguoiDung] ([ID_TaiKhoan], [TaiKhoan], [MatKhau], [Ten], [NamSinh], [SDT], [DiaChi], [isAdmin]) VALUES (4, N'testmuahang', N'03a979face1a49339d090440529d291e', N'test mua hangf', N'1999', N'0876522222', N'Dong Tháp', 0)
SET IDENTITY_INSERT [dbo].[ThongTinNguoiDung] OFF
GO
ALTER TABLE [dbo].[ChiTietBan]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietBan_HoaDonBan] FOREIGN KEY([ID_HoaDonBan])
REFERENCES [dbo].[HoaDonBan] ([ID_HoaDonBan])
GO
ALTER TABLE [dbo].[ChiTietBan] CHECK CONSTRAINT [FK_ChiTietBan_HoaDonBan]
GO
ALTER TABLE [dbo].[ChiTietBan]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietBan_SanPham] FOREIGN KEY([ID_SanPham])
REFERENCES [dbo].[SanPham] ([ID_SanPham])
GO
ALTER TABLE [dbo].[ChiTietBan] CHECK CONSTRAINT [FK_ChiTietBan_SanPham]
GO
ALTER TABLE [dbo].[ChiTietMua]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietMua_HoaDonMua] FOREIGN KEY([ID_HoaDonMua])
REFERENCES [dbo].[HoaDonMua] ([ID_HoaDonMua])
GO
ALTER TABLE [dbo].[ChiTietMua] CHECK CONSTRAINT [FK_ChiTietMua_HoaDonMua]
GO
ALTER TABLE [dbo].[ChiTietMua]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietMua_SanPham] FOREIGN KEY([ID_SanPham])
REFERENCES [dbo].[SanPham] ([ID_SanPham])
GO
ALTER TABLE [dbo].[ChiTietMua] CHECK CONSTRAINT [FK_ChiTietMua_SanPham]
GO
ALTER TABLE [dbo].[GioHang]  WITH CHECK ADD  CONSTRAINT [FK_GioHang_SanPham] FOREIGN KEY([ID_SanPham])
REFERENCES [dbo].[SanPham] ([ID_SanPham])
GO
ALTER TABLE [dbo].[GioHang] CHECK CONSTRAINT [FK_GioHang_SanPham]
GO
ALTER TABLE [dbo].[GioHang]  WITH CHECK ADD  CONSTRAINT [FK_GioHang_TaiKhoan] FOREIGN KEY([ID_TaiKhoan])
REFERENCES [dbo].[ThongTinNguoiDung] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[GioHang] CHECK CONSTRAINT [FK_GioHang_TaiKhoan]
GO
ALTER TABLE [dbo].[HoaDonBan]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonBan_TaiKhoan] FOREIGN KEY([ID_TaiKhoanAdmin])
REFERENCES [dbo].[ThongTinNguoiDung] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[HoaDonBan] CHECK CONSTRAINT [FK_HoaDonBan_TaiKhoan]
GO
ALTER TABLE [dbo].[HoaDonBan]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonBan_TaiKhoan1] FOREIGN KEY([ID_TaiKhoanKH])
REFERENCES [dbo].[ThongTinNguoiDung] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[HoaDonBan] CHECK CONSTRAINT [FK_HoaDonBan_TaiKhoan1]
GO
ALTER TABLE [dbo].[HoaDonMua]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonMua_NhaPhanPhoi] FOREIGN KEY([ID_NhaPhanPhoi])
REFERENCES [dbo].[NhaPhanPhoi] ([ID_NhaPhanPhoi])
GO
ALTER TABLE [dbo].[HoaDonMua] CHECK CONSTRAINT [FK_HoaDonMua_NhaPhanPhoi]
GO
ALTER TABLE [dbo].[HoaDonMua]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonMua_TaiKhoan] FOREIGN KEY([ID_TaiKhoan])
REFERENCES [dbo].[ThongTinNguoiDung] ([ID_TaiKhoan])
GO
ALTER TABLE [dbo].[HoaDonMua] CHECK CONSTRAINT [FK_HoaDonMua_TaiKhoan]
GO
ALTER TABLE [dbo].[KhoHang]  WITH CHECK ADD  CONSTRAINT [FK_KhoHang_SanPham] FOREIGN KEY([ID_SanPham])
REFERENCES [dbo].[SanPham] ([ID_SanPham])
GO
ALTER TABLE [dbo].[KhoHang] CHECK CONSTRAINT [FK_KhoHang_SanPham]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_PhanLoai] FOREIGN KEY([ID_PhanLoai])
REFERENCES [dbo].[PhanLoai] ([ID_PhanLoai])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_PhanLoai]
GO
/****** Object:  StoredProcedure [dbo].[CHECKID]    Script Date: 2021-11-07 7:07:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CHECKID] @checkid_tk int, @checkid_sp int, @soluong int
AS
BEGIN
 if exists (select * from GioHang where ID_TaiKhoan = @checkid_tk and ID_SanPham = @checkid_sp)
			BEGIN
					UPDATE GioHang set [SoLuongTrongGioHang(KG)] = [SoLuongTrongGioHang(KG)] + @soluong where ID_TaiKhoan = @checkid_tk and ID_SanPham = @checkid_sp
			END
else
			BEGIN
					INSERT INTO GioHang Values(@checkid_tk, @checkid_sp,@soluong)
			END
END			
GO
/****** Object:  Trigger [dbo].[trigg_AutoDateSell]    Script Date: 2021-11-07 7:07:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[trigg_AutoDateSell] on [dbo].[HoaDonBan] after insert as
begin
	update HoaDonBan 
	set NgayLapHoaDon = GETDATE(),
		TrangThai = '0'
	where HoaDonBan.ID_HoaDonBan = (SELECT TOP (1) ID_HoaDonBan FROM HoaDonBan order by ID_HoaDonBan desc)
end
GO
ALTER TABLE [dbo].[HoaDonBan] ENABLE TRIGGER [trigg_AutoDateSell]
GO
/****** Object:  Trigger [dbo].[trigg_AutoDateBuy]    Script Date: 2021-11-07 7:07:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[trigg_AutoDateBuy] on [dbo].[HoaDonMua] after insert as
begin
	update HoaDonMua set NgayLapHoaDon = GETDATE()
	where HoaDonMua.ID_HoaDonMua = (SELECT TOP (1) ID_HoaDonMua FROM HoaDonMua order by ID_HoaDonMua desc)
end
GO
ALTER TABLE [dbo].[HoaDonMua] ENABLE TRIGGER [trigg_AutoDateBuy]
GO
/****** Object:  Trigger [dbo].[trigg_AutoDetele]    Script Date: 2021-11-07 7:07:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create trigger [dbo].[trigg_AutoDetele] on [dbo].[SanPham] after insert as
begin
	update SanPham 
	set Deleted = 0
	where ID_SanPham = (SELECT TOP (1) ID_SanPham FROM SanPham order by ID_SanPham desc)
end
GO
ALTER TABLE [dbo].[SanPham] ENABLE TRIGGER [trigg_AutoDetele]
GO
USE [master]
GO
ALTER DATABASE [Fruits_Shop] SET  READ_WRITE 
GO
