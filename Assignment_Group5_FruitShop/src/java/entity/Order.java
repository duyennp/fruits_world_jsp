/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Date;

/**
 *
 * @author nhóm 5
 */
public class Order {

    private String name;
    private Date day;
    private String photo;
    private String namefruit;
    private int quantity;
    private int price;

    public Order() {
    }

    public Order(String name, Date day, String photo, String namefruit, int quantity, int price) {
        this.name = name;
        this.day = day;
        this.photo = photo;
        this.namefruit = namefruit;
        this.quantity = quantity;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getNamefruit() {
        return namefruit;
    }

    public void setNamefruit(String namefruit) {
        this.namefruit = namefruit;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Order{" + "name=" + name + ", day=" + day + ", photo=" + photo + ", namefruit=" + namefruit + ", quantity=" + quantity + ", price=" + price + '}';
    }

}
