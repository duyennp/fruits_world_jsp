/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author nhóm 5
 */
public class ChiTietMua {

    private String id;
    private String quantity;
    private String price;

    public ChiTietMua(String id, String quantity, String price) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
    }

    public ChiTietMua() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ChiTietMua{" + "id=" + id + ", quantity=" + quantity + ", price=" + price + '}';
    }
}
