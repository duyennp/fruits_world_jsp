/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author nhóm 5
 */
public class KhoHang {

    private String id;
    private int quantity_kho;

    public KhoHang(String id, int quantity_kho) {
        this.id = id;
        this.quantity_kho = quantity_kho;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuantity_kho() {
        return quantity_kho;
    }

    public void setQuantity_kho(int quantity_kho) {
        this.quantity_kho = quantity_kho;
    }

    @Override
    public String toString() {
        return "KhoHang{" + "id=" + id + ", quantity_kho=" + quantity_kho + '}';
    }

}
