/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Date;

/**
 *
 * @author nhóm 5
 */
public class Cart {

    private String id;
    private String image;
    private String name;
    private int quantity;
    private int price;
    private Date date;

    public Cart(String id, String image, String name, int quantity, int price, Date date) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.date = date;
    }

    public Cart(String id, String image, String name, int quantity, int price) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Cart() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}
