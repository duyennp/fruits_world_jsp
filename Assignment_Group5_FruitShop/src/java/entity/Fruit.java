/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author nhóm 5
 */
public class Fruit {

    private int id;
    private String name;
    private String photo;
    private String infor;
    private int id_type;
    private String name_Type;
    private int quantity;
    private int price;

    public Fruit() {
    }

    public Fruit(int id, String name, String photo, String infor, int id_type, String name_Type, int quantity, int price) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.infor = infor;
        this.id_type = id_type;
        this.name_Type = name_Type;
        this.quantity = quantity;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getInfor() {
        return infor;
    }

    public void setInfor(String infor) {
        this.infor = infor;
    }

    public int getId_type() {
        return id_type;
    }

    public void setId_type(int id_type) {
        this.id_type = id_type;
    }

    public String getName_Type() {
        return name_Type;
    }

    public void setName_Type(String name_Type) {
        this.name_Type = name_Type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Fruit{" + "id=" + id + ", name=" + name + ", photo=" + photo + ", infor=" + infor + ", id_type=" + id_type + ", name_Type=" + name_Type + ", quantity=" + quantity + ", price=" + price + '}';
    }

}
