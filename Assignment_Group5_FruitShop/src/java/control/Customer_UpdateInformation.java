/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.CustomerDAO;
import entity.Account;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nhóm 5
 */
public class Customer_UpdateInformation extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         request.setCharacterEncoding("utf-8");

        //Thay đổi thông tin khách hàng
        String username = request.getParameter("username");
        String yourname = request.getParameter("yourname");
        String birth = request.getParameter("birth");
        String sdt = request.getParameter("sdt");
        String address = request.getParameter("address");

        if (yourname.trim().isEmpty() || birth.trim().isEmpty() || sdt.trim().isEmpty() || address.trim().isEmpty()) {
            request.setAttribute("Ok", "Thông Tin Điền Vào Không Được Để Trống");
            request.getRequestDispatcher("Customer_UpdateInformation.jsp").forward(request, response);
        } else if (username.replaceAll("\\s\\s+", " ").trim().length() >= 8) {
            String checkInt = "^[[0-9]+\\s?]+$";
            
            Pattern patternInt = Pattern.compile(checkInt);
            String testBirth = birth.replaceAll("\\s\\s+", " ").trim();
            
            if (!patternInt.matcher(testBirth).matches()) {
                request.setAttribute("Ok", "Năm Sinh Chỉ Được Sử Dụng Chữ Số");
                request.getRequestDispatcher("Customer_UpdateInformation.jsp").forward(request, response);
            } else {
                HttpSession session = request.getSession();
                CustomerDAO dao = new CustomerDAO();
                dao.updateAccount(username, yourname, birth, sdt, address);
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Customer_UpdateInformation.class.getName()).log(Level.SEVERE, null, ex);
                }

                session.removeAttribute("acc");
                request.setAttribute("color", "green");
                request.setAttribute("Ok", "Thông Tin Qúy Khách Đã Được Thay Đổi, Vui Lòng Đăng Nhập Lại");
                request.getRequestDispatcher("Page_Login.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        //Thay đổi mật khảu
        String oldpass = request.getParameter("oldpassword");
        String newpass = request.getParameter("newpassword");
        String renewpass = request.getParameter("renewpassword");

        CustomerDAO dao = new CustomerDAO();
        String checkoldpass = dao.MD5Encryption(oldpass);
        
        request.setAttribute("oldpass", oldpass);
        request.setAttribute("newpass", newpass);
        request.setAttribute("renewpass", renewpass);
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");

        if (!checkoldpass.equals(a.getPassword())) {
            request.setAttribute("Ok", "Mật Khẩu Hiện Tại Không Đúng");
            request.getRequestDispatcher("Customer_ChangePassword.jsp").forward(request, response);
        } else {
            if (newpass.trim().isEmpty() || renewpass.trim().isEmpty()) {
                request.setAttribute("Ok", "Thông Tin Điền Vào Không Được Để Trống");
                request.getRequestDispatcher("Customer_ChangePassword.jsp").forward(request, response);
            } else if (newpass.replaceAll("\\s\\s+", " ").trim().length() < 8) {
                request.setAttribute("Ok", "Tài Khoản và Mật Khẩu cần 8 kí tự trở lên");
                request.getRequestDispatcher("Customer_ChangePassword.jsp").forward(request, response);
            } else if (newpass.replaceAll("\\s\\s+", " ").trim().length() >= 8) {
                String checkString = "^[[a-zA-Z0-9]+\\s?]+$";
                Pattern patternString = Pattern.compile(checkString);
                String testPassword = newpass.replaceAll("\\s\\s+", " ").trim();
                if (!patternString.matcher(testPassword).matches()) {//kiểm tra có khớp vói format không
                    request.setAttribute("Ok", "Mật Khẩu Chỉ được sử dụng kí tự và chữ số");
                    request.getRequestDispatcher("Customer_ChangePassword.jsp").forward(request, response);
                } else {
                    if (!newpass.equals(renewpass)) {
                        request.setAttribute("Ok", "Nhập Lại Mật Khẩu Không Khớp");
                        request.getRequestDispatcher("Customer_ChangePassword.jsp").forward(request, response);
                    } else {
                        dao.updateAccountPassword(a.getUsername(), newpass);

                        session.removeAttribute("acc");
                        request.setAttribute("color", "green");
                        request.setAttribute("Ok", "Mật Khẩu Của Qúy Khách Đã Được Thay Đổi, Vui Lòng Đăng Nhập Lại");
                        request.getRequestDispatcher("Page_Login.jsp").forward(request, response);
                    }
                }
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
