/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.CartDAO;
import entity.Account;
import entity.Cart;
import entity.KhoHang;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nhóm 5
 */
public class Cart_Purchase extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         request.setCharacterEncoding("utf-8");
        
        //thanh toán cart
        HttpSession session = request.getSession();
        CartDAO dao = new CartDAO();
        Account a = (Account) session.getAttribute("acc");
        List<Cart> list = dao.getAllCart(a.getId());
        List<KhoHang> listKho = dao.checkQuantity();
        HashMap<String, Integer> listNotEnough = new HashMap<String, Integer>();
        int check = 0;
        for (Cart cart : list) {
            for (KhoHang kho : listKho) {
                if (cart.getId().equals(kho.getId())) {
                    int quantity = kho.getQuantity_kho() - cart.getQuantity();
                    if (quantity < 0) {
                        check = 1;
                        listNotEnough.put(cart.getName(), kho.getQuantity_kho());

                    }
                }
            }
        }
        if (check == 0) {
            for (Cart cart : list) {
                for (KhoHang kho : listKho) {
                    if (cart.getId().equals(kho.getId())) {
                        dao.updateQuantity(cart.getQuantity(), kho.getId());
                    }
                }

            }
            dao.PayCart(a.getId(), list);
            response.sendRedirect("Cart_Success.jsp");
        } else {
            request.setAttribute("sp", listNotEnough);
            request.getRequestDispatcher("Cart_Error.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
