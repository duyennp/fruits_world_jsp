/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.CartDAO;
import entity.Account;
import entity.Cart;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nhóm 5
 */
public class Cart_Show extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
          //hiện thị dữ liệu trong cart
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");

        CartDAO dao = new CartDAO();
        List<Cart> list = dao.getAllCart(a.getId());

        int sum = 0;

        for (Cart cart : list) {
            sum += cart.getPrice() * cart.getQuantity();
        }
        request.setAttribute("sale", 0);
        request.setAttribute("sum", sum);
        request.setAttribute("list", list);
        request.getRequestDispatcher("Cart.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          String code = request.getParameter("code").replaceAll("\\s\\s+", " ").trim();
          // Lấy mã giảm giá
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");

        CartDAO dao = new CartDAO();
        List<Cart> list = dao.getAllCart(a.getId());

        int sum = 0, sale = 0;
        if (code.equalsIgnoreCase("FruitsShop")) {
            for (Cart cart : list) {
                sum += cart.getPrice() * cart.getQuantity();
            }
            sale = (int) sum * 20 / 100;
            request.setAttribute("Mess", "Áp Dụng Mã Giảm Giá Thành Công, Quý Khách Được Giảm 20% Số Tiền");
            request.setAttribute("sale", sale);
            request.setAttribute("sum", sum);
            request.setAttribute("list", list);
            request.getRequestDispatcher("Cart.jsp").forward(request, response);

        } else {
            processRequest(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
