/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.DistributorsDAO;
import dao.FruitsDAO;
import entity.Distributors;
import entity.Fruit;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nhóm 5
 */
public class Admin_ShowDistributor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         request.setCharacterEncoding("utf-8");

        //chức năng dự vào biến type
        String type = request.getParameter("type");

        //thêm nhà phân phối mới
        if (type.contains("addNew")) {
            String nameDis = request.getParameter("nameDis");
            String addressDis = request.getParameter("addressDis");
            String sdtDis = request.getParameter("sdtDis");
            DistributorsDAO disDAO = new DistributorsDAO();
            disDAO.addDistributors(nameDis, sdtDis, addressDis);
            request.getRequestDispatcher("Admin_Manager.jsp").forward(request, response);
        }

        //xác nhận số lượng nhập hàng từ nhà phân phối
        if (type.contains("comfirmFruits")) {
            String idNPP = request.getParameter("idNPP");
            String quantity = request.getParameter("quantity");
            HttpSession sess = request.getSession();

            sess.setAttribute("idNPP", idNPP);
            sess.setAttribute("quantity", quantity);

            DistributorsDAO disDAO = new DistributorsDAO();
            Distributors o = disDAO.getDistributorsByID(idNPP);
            FruitsDAO listDAO = new FruitsDAO();
            List<Fruit> listFruits = listDAO.getInforAllFruits();

            request.setAttribute("distributors", o);
            request.setAttribute("listF", listFruits);
            request.setAttribute("quantityFruit", quantity);
            request.getRequestDispatcher("Admin_AddFruitsList.jsp").forward(request, response);
        }

        //update thông tin Nhà Phân Phối
        if (type.contains("updateNPP")) {
            String did = request.getParameter("did");
            DistributorsDAO dao = new DistributorsDAO();
            Distributors dis = dao.getDistributorsByID(did);
            request.setAttribute("distributor", dis);
            request.getRequestDispatcher("Admin_UpdateNPP.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
