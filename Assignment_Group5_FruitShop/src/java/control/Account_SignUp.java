/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.LoginDAO;
import entity.Account;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nhóm 5
 */
public class Account_SignUp extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");

        //sign up thôg tin khách hàng
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String repassword = request.getParameter("repassword");
        String yourname = request.getParameter("yourname");
        String birth = request.getParameter("birth");
        String sdt = request.getParameter("sdt");
        String address = request.getParameter("address");
        request.setAttribute("username", username);        
        request.setAttribute("password", password);
        request.setAttribute("repassword", repassword);
        request.setAttribute("yourname", yourname);
        request.setAttribute("birth", birth);
        request.setAttribute("sdt", sdt);
        request.setAttribute("address", address);
                
        
        if (username.trim().isEmpty() || password.trim().isEmpty() || repassword.trim().isEmpty()
                || yourname.trim().isEmpty() || birth.trim().isEmpty() || sdt.trim().isEmpty() || address.trim().isEmpty()) {
            request.setAttribute("Pass", "Thông Tin Điền Vào Không Được Để Trống");
            request.getRequestDispatcher("Page_SignUp.jsp").forward(request, response);
        } else if (username.replaceAll("\\s\\s+", " ").trim().length() < 8 || password.replaceAll("\\s\\s+", " ").trim().length() < 8) {
            request.setAttribute("Pass", "Tài Khoản và Mật Khẩu cần 8 kí tự trở lên");
            request.getRequestDispatcher("Page_SignUp.jsp").forward(request, response);
        } else if (username.replaceAll("\\s\\s+", " ").trim().length() >= 8 && password.replaceAll("\\s\\s+", " ").trim().length() >= 8) {
            String checkString = "^[[a-zA-Z0-9]+\\s?]+$";
            String checkInt = "^[[0-9]+\\s?]+$";
            Pattern patternString = Pattern.compile(checkString);
            Pattern patternInt = Pattern.compile(checkInt);
            String testUsername = username.replaceAll("\\s\\s+", " ").trim();
            String testPassword = password.replaceAll("\\s\\s+", " ").trim();
            String testBirth = birth.replaceAll("\\s\\s+", " ").trim();
            if (!patternInt.matcher(testBirth).matches()) {
                request.setAttribute("Pass", "Năm Sinh Chỉ Được Sử Dụng Chữ Số");
                request.getRequestDispatcher("Page_SignUp.jsp").forward(request, response);
            } else if (!patternString.matcher(testUsername).matches() || !patternString.matcher(testPassword).matches()) {//kiểm tra có khớp vói format không
                request.setAttribute("Pass", "Tài Khoản và Mật Khẩu Chỉ được sử dụng kí tụ và chữ số");
                request.getRequestDispatcher("Page_SignUp.jsp").forward(request, response);
            } else {
                if (!password.equals(repassword)) {
                    request.setAttribute("Pass", "Mật Khẩu Không Khớp Nhau");
                    request.getRequestDispatcher("Page_SignUp.jsp").forward(request, response);
                } else {
                    LoginDAO dao = new LoginDAO();
                    Account a = dao.checkAccountExist(username);
                    if (a == null) {
                        dao.createAccount(username, password, yourname, birth, sdt, address, 0);
                        request.setAttribute("color", "green");
                        request.setAttribute("Ok", "Đăng kí Thành Công, Quý Khách Có Thể Đăng Nhập Và Trải Nghiệm");
                        request.getRequestDispatcher("Page_Login.jsp").forward(request, response);
                    } else {
                        request.setAttribute("User", "Tài Khoản Tồn Tại");
                        request.getRequestDispatcher("Page_SignUp.jsp").forward(request, response);
                    }
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Account_SignUp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Account_SignUp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
