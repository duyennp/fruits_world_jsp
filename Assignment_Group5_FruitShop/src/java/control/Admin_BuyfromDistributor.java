/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.DistributorsDAO;
import entity.Account;
import entity.ChiTietMua;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nhóm 5
 */
public class Admin_BuyfromDistributor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
          request.setCharacterEncoding("utf-8");
        
        //thêm danh sách trái cây từ nhà phân phối
        HttpSession sess = request.getSession();
        String idNPP = (String) sess.getAttribute("idNPP");
        String soLoai = (String) sess.getAttribute("quantity");
        List<ChiTietMua> listFruitsBuy = new ArrayList<>();
        Account acc = (Account) sess.getAttribute("acc");

        //vong lặp lấy danh sach trai cay
        for (int i = 1;; i++) {
            String idFruit = "nameFruits" + i;
            String quantityFruit = "quantity" + i;
            String priceFruit = "priceFruit" + i;
            String id = request.getParameter(idFruit);
            String quantity = request.getParameter(quantityFruit);
            String price = request.getParameter(priceFruit);
            if (id == null) {
                break;
            }
            listFruitsBuy.add(new ChiTietMua(id, quantity, price));
        }

        DistributorsDAO dao = new DistributorsDAO();
        dao.insertChiTietMua(acc.getId(), idNPP, soLoai, listFruitsBuy);
        response.sendRedirect("Admin_Manager.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
