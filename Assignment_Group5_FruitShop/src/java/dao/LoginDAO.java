/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import entity.Account;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author nhóm 5
 */
public class LoginDAO {

    Connection conn = null; // ket noi sql
    PreparedStatement ps = null; // nem lenh vao sql
    ResultSet rs = null; //nhan ket qua tra ve tu sql

    //check đã login chưa
    public Account checkLogin(String user, String pass) throws Exception {
        try {
            String query = "select * from ThongTinNguoiDung where TaiKhoan = ? and MatKhau = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, user);
            ps.setString(2, MD5Encryption(pass));
            rs = ps.executeQuery();
            while (rs.next()) {
                Account a = new Account(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8));
                return a;
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    //check tài khoản tồn tại
    public Account checkAccountExist(String user) throws Exception {
        try {
            String query = "select * from ThongTinNguoiDung where TaiKhoan = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, user);
            rs = ps.executeQuery();
            while (rs.next()) {
                Account a = new Account(rs.getString(1), rs.getString(2));
                return a;
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    //tạo tài khoản mới
    public void createAccount(String username, String password, String yourname, String birth, String sdt, String address, int isAdmin) {
        String query = "insert into ThongTinNguoiDung\n"
                + "values(?,?,?,?,?,?,?)";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, MD5Encryption(password));
            ps.setString(3, yourname);
            ps.setString(4, birth);
            ps.setString(5, sdt);
            ps.setString(6, address);
            ps.setInt(7, 0);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
     //Mã hóa mật khẩu bắng md5
    public String MD5Encryption(String password) {//Phương thức mã hóa password thành mã băm
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");//Phương thức getInstance tĩnh được gọi với tên MD5
            md.update(password.getBytes());//Sử dụng hàm update để lấy byte của password
            return DatatypeConverter.printHexBinary(md.digest()).toLowerCase();//cuối cùng chúng ta cần sử dụng hàm digest() để tạo mã băm:
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();//In ra lỗi, nếu có
        }
        return null;
    }
}
