/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import entity.Classify;
import entity.Fruit;
import entity.Order;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nhóm 5
 */
public class FruitsDAO {

    Connection conn = null; // ket noi sql
    PreparedStatement ps = null; // nem lenh vao sql
    ResultSet rs = null; //nhan ket qua tra ve tu sql

    //lấy thông tin danh sách trái cây
    public List<Fruit> getInforAllFruits() {
        List<Fruit> list = new ArrayList<>();
        String query = "select sp.ID_SanPham, sp.TenSanPham, sp.AnhSanPham, sp.ThongTinSanPham, pl.ID_PhanLoai, pl.TenPhanLoai, kh.[SoLuongTonKho(KG)], kh.[GiaBan(VND)] \n"
                + "from SanPham sp, PhanLoai pl, KhoHang kh\n"
                + "where sp.ID_PhanLoai = pl.ID_PhanLoai\n"
                + "and sp.ID_SanPham = kh.ID_SanPham \n"
                + "and sp.Deleted = 0 \n"
                + "order by sp.ID_SanPham desc";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Fruit(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //lấy thôngtin trái cây bằng id
    public Fruit getInforFruitByID(String fid) {
        String query = "select sp.ID_SanPham, sp.TenSanPham, sp.AnhSanPham, sp.ThongTinSanPham, pl.ID_PhanLoai, pl.TenPhanLoai, kh.[SoLuongTonKho(KG)], kh.[GiaBan(VND)]\n"
                + "from SanPham sp, PhanLoai pl, KhoHang kh\n"
                + "where sp.ID_PhanLoai = pl.ID_PhanLoai\n"
                + "and sp.ID_SanPham = kh.ID_SanPham\n"
                + "and sp.ID_SanPham = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, fid);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Fruit(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getInt(8));
            }
        } catch (Exception e) {
        }
        return null;
    }

    // them trái cây
    public void insertFruit(String name, String photo, String infor, String price) {
        int priceF = Integer.parseInt(price);
        String pl = "1";
        if (priceF > 50000 && priceF <= 200000) {
            pl = "2";
        } else if (priceF > 200000 && priceF <= 500000) {
            pl = "3";
        } else if (priceF > 500000 && priceF <= 1000000) {
            pl = "4";
        } else if (priceF > 1000000) {
            pl = "5";
        }
        String query = "insert into SanPham (TenSanPham, AnhSanPham, ThongTinSanPham, ID_PhanLoai)\n"
                + "values (?,?,N'" + infor + "',?)\n"
                + "insert into KhoHang(ID_SanPham, [SoLuongTonKho(KG)], [GiaBan(VND)])\n"
                + "values ((select top (1) ID_SanPham from SanPham order by ID_SanPham desc),0,?)";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, name);
            ps.setString(2, photo);
            ps.setString(3, pl);
            ps.setString(4, price);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //update thông tin trái cây
    public void updateFruit(String fid, String photoFruit, String motaFruit, String plid, String quantity, String price) {
        //motaFruit += "N'" + motaFruit +"'";
        String query = "update SanPham\n"
                + "set AnhSanPham = ?,\n"
                + "ThongTinSanPham = N'" + motaFruit + "',\n"
                + "ID_PhanLoai = ?\n"
                + "where ID_SanPham = ?\n"
                + "\n"
                + "update KhoHang \n"
                + "set [SoLuongTonKho(KG)] = ?,\n"
                + "[GiaBan(VND)] = ?\n"
                + "where ID_SanPham = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, photoFruit);
            ps.setString(2, plid);
            ps.setString(3, fid);
            ps.setString(4, quantity);
            ps.setString(5, price);
            ps.setString(6, fid);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //lấy list trái cây theo phân loại
    public List<Fruit> getListFruitByIDClassify(String plid) {
        List<Fruit> list = new ArrayList<>();
        String query = "select sp.ID_SanPham, sp.TenSanPham, sp.AnhSanPham, sp.ThongTinSanPham, pl.ID_PhanLoai, pl.TenPhanLoai, kh.[SoLuongTonKho(KG)], kh.[GiaBan(VND)] \n"
                + "from SanPham sp, PhanLoai pl, KhoHang kh\n"
                + "where sp.ID_PhanLoai = pl.ID_PhanLoai\n"
                + "and sp.ID_SanPham = kh.ID_SanPham\n"
                + "and sp.Deleted = 0 \n"
                + "and pl.ID_PhanLoai = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, plid);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Fruit(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //thêm trái cây vào cart
    public void addCart(String AccountID, String FruitID, String quantity) {
        String query = "execute CHECKID ?,?,?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, AccountID);
            ps.setString(2, FruitID);
            ps.setString(3, quantity);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //lấy list order của khách hàng cho admin
    public List<List<Order>> getListOrder() {
        String queryTong = "select ID_HoaDonBan from HoaDonBan where TrangThai = 0";
        List<Integer> sumHDB = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(queryTong);
            rs = ps.executeQuery();
            while (rs.next()) {
                sumHDB.add(rs.getInt(1));
            }
        } catch (Exception e) {
        }

        List<List<Order>> listF = new ArrayList<>();

        for (Integer shd : sumHDB) {

            String query = "select ttnd.TaiKhoan, hdb.NgayLapHoaDon, sp.AnhSanPham, sp.TenSanPham, ctb.[SoLuongBan(KG)], ctb.[GiaBan(VND)] from HoaDonBan hdb, ChiTietBan ctb, ThongTinNguoiDung ttnd, SanPham sp\n"
                    + "where hdb.ID_HoaDonBan = ctb.ID_HoaDonBan\n"
                    + "and hdb.ID_TaiKhoanKH = ttnd.ID_TaiKhoan\n"
                    + "and ctb.ID_SanPham = sp.ID_SanPham\n"
                    + "and ctb.ID_HoaDonBan = " + shd;
            try {
                conn = new DBContext().getConnection();
                ps = conn.prepareStatement(query);
                rs = ps.executeQuery();
                List<Order> listOrder = new ArrayList<>();
                while (rs.next()) {
                    listOrder.add(new Order(rs.getString(1),
                            rs.getDate(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getInt(5),
                            rs.getInt(6)));
                }
                listF.add(listOrder);
            } catch (Exception e) {
            }

        }

        return listF;
    }

    //lấy danh sách phân loại
    public List<Classify> getAllClassify() {
        List<Classify> list = new ArrayList<>();
        String query = "select * from PhanLoai";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Classify(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
        }

        return list;
    }

    //search trái cây theo tên
    public List<Fruit> getFruitByName(String namef) {
        List<Fruit> list = new ArrayList<>();
        String query = "select sp.ID_SanPham, sp.TenSanPham, sp.AnhSanPham, sp.ThongTinSanPham, pl.ID_PhanLoai, pl.TenPhanLoai, kh.[SoLuongTonKho(KG)], kh.[GiaBan(VND)]\n"
                + "from SanPham sp, PhanLoai pl, KhoHang kh\n"
                + "where sp.ID_PhanLoai = pl.ID_PhanLoai\n"
                + "and sp.ID_SanPham = kh.ID_SanPham\n"
                + "and sp.Deleted = 0 \n"
                + "and sp.TenSanPham like '%" + namef + "%'";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Fruit(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //lấy số lượng đã bán của trái cây theo id
    public Integer getQuantitySellByID(String fid) {
        String query = "select sum(ctb.[SoLuongBan(KG)]) as soluongban from ChiTietBan ctb, SanPham sp\n"
                + "where ctb.ID_SanPham = sp.ID_SanPham\n"
                + "and sp.ID_SanPham = " + fid;
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return null;
    }

    //xóa trai cây trong giỏ hàng
    public void deleteFruit(String fid) {
        String query = "update SanPham\n"
                + "set Deleted = 1\n"
                + "where ID_SanPham = " + fid;
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //tong danh sach fruit
    public Integer sumListFruit() {
        String query = "select count(*) from SanPham";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //tong danh sach fruit theo phan loai
    public Integer sumListFruitByClassify(String classify) {
        String query = "select count(*) from SanPham where ID_PhanLoai = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, classify);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //tong danh sach fruit theo search ten
    public Integer sumListFruitByFruitName(String fname) {
        String query = "select count(*) from SanPham where TenSanPham like ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, "%" + fname + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //lay danh sach fruit theo index
    public List<Fruit> getListFruitsByIndex(String index) {
        List<Fruit> list = new ArrayList<>();
        String query = "select sp.ID_SanPham, sp.TenSanPham, sp.AnhSanPham, sp.ThongTinSanPham, pl.ID_PhanLoai, pl.TenPhanLoai, kh.[SoLuongTonKho(KG)], kh.[GiaBan(VND)]\n"
                + "from SanPham sp, PhanLoai pl, KhoHang kh\n"
                + "where sp.ID_PhanLoai = pl.ID_PhanLoai\n"
                + "and sp.ID_SanPham = kh.ID_SanPham \n"
                + "and sp.Deleted = 0 \n"
                + "order by ID_SanPham desc\n"
                + "offset ? row fetch next 9 row only";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, (Integer.parseInt(index) - 1) * 9);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Fruit(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //lay danh sach fruit theo index va phan loai
    public List<Fruit> getListFruitsByIndexAndClassify(String index, String pl) {
        List<Fruit> list = new ArrayList<>();
        String query = "select sp.ID_SanPham, sp.TenSanPham, sp.AnhSanPham, sp.ThongTinSanPham, pl.ID_PhanLoai, pl.TenPhanLoai, kh.[SoLuongTonKho(KG)], kh.[GiaBan(VND)]\n"
                + "from SanPham sp, PhanLoai pl, KhoHang kh\n"
                + "where sp.ID_PhanLoai = pl.ID_PhanLoai\n"
                + "and sp.ID_SanPham = kh.ID_SanPham \n"
                + "and sp.Deleted = 0\n"
                + "and sp.ID_PhanLoai = ?\n"
                + "order by ID_SanPham desc\n"
                + "offset ? row fetch next 9 row only";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, pl);
            ps.setInt(2, (Integer.parseInt(index) - 1) * 9);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Fruit(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //lay danh sach fruit theo index va search fname
    public List<Fruit> getListFruitsByIndexAndFruitName(String index, String fname) {
        List<Fruit> list = new ArrayList<>();
        String query = "select sp.ID_SanPham, sp.TenSanPham, sp.AnhSanPham, sp.ThongTinSanPham, pl.ID_PhanLoai, pl.TenPhanLoai, kh.[SoLuongTonKho(KG)], kh.[GiaBan(VND)]\n"
                + "from SanPham sp, PhanLoai pl, KhoHang kh\n"
                + "where sp.ID_PhanLoai = pl.ID_PhanLoai\n"
                + "and sp.ID_SanPham = kh.ID_SanPham \n"
                + "and sp.Deleted = 0\n"
                + "and sp.TenSanPham like ?\n"
                + "order by ID_SanPham desc\n"
                + "offset ? row fetch next 9 row only";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, fname);
            ps.setInt(2, (Integer.parseInt(index) - 1) * 9);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Fruit(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public static void main(String[] args) {
        FruitsDAO dao = new FruitsDAO();
        List<Fruit> list = dao.getListFruitsByIndexAndClassify("1", "1");
        for (Fruit o : list) {
            System.out.println(o);
        }
    }
}
