/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import entity.ChiTietMua;
import entity.Distributors;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nhóm 5
 */
public class DistributorsDAO {

    Connection conn = null; // ket noi sql
    PreparedStatement ps = null; // nem lenh vao sql
    ResultSet rs = null; //nhan ket qua tra ve tu sql

    //lấy danh sách nhà phân phối
    public List<Distributors> getAllDistributors() {
        List<Distributors> list = new ArrayList<>();

        try {
            String query = "select * from NhaPhanPhoi where Deleted = 0";
            conn = new DBContext().getConnection(); //ket noi
            ps = conn.prepareStatement(query); //nem cau lenh
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Distributors(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    // thêm nhà phân phối
    public void addDistributors(String name, String sdt, String address) {
        try {
            String query = "insert into NhaPhanPhoi\n"
                    + "values (N'" + name + "',N'" + sdt + "',N'" + address + "',0)";
            conn = new DBContext().getConnection(); //ket noi
            ps = conn.prepareStatement(query); //nem cau lenh
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //lấy nhà phân phối theo id
    public Distributors getDistributorsByID(String id) {
        try {
            String query = "select * from NhaPhanPhoi \n"
                    + "where ID_NhaPhanPhoi = ?";
            conn = new DBContext().getConnection(); //ket noi
            ps = conn.prepareStatement(query); //nem cau lenh
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Distributors(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4));
            }
        } catch (Exception e) {
        }
        return null;
    }

    //them chi tiết mua của nhà phân phối
    public void insertChiTietMua(String id_taiKhoan, String id_npp, String soLoai, List<ChiTietMua> listFruits) {
        int loai = Integer.parseInt(soLoai);
        String query = " insert into HoaDonMua (ID_TaiKhoan, ID_NhaPhanPhoi)\n"
                + "values ( " + id_taiKhoan + " , " + id_npp + ") ";

        for (int i = 0; i < loai; i++) {
            query += "\n"
                    + "insert into ChiTietMua (ID_HoaDonMua, ID_SanPham, [SoLuongMua(KG)], [GiaMua(VND)]) \n"
                    + "values ((select top (1) ID_HoaDonMua from HoaDonMua order by ID_HoaDonMua desc)," + listFruits.get(i).getId() + "," + listFruits.get(i).getQuantity() + "," + listFruits.get(i).getPrice() + ") \n"
                    + " \n"
                    + "update KhoHang set [SoLuongTonKho(KG)] = [SoLuongTonKho(KG)] + " + listFruits.get(i).getQuantity() + "\n"
                    + "where KhoHang.ID_SanPham = " + listFruits.get(i).getId() + " ";
        }

        try {
            conn = new DBContext().getConnection(); //ket noi
            ps = conn.prepareStatement(query); //nem cau lenh
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //update thông tin nhà phân phối
    public void updateInforDis(String disid, String disname, String disphone, String disaddress) {
        String query = "update NhaPhanPhoi\n"
                + "set TenNhaPhanPhoi = N'" + disname + "',\n"
                + "SDT = N'" + disphone + "',\n"
                + "DiaChi = N'" + disaddress + "'\n"
                + "where ID_NhaPhanPhoi = " + disid;
        try {
            conn = new DBContext().getConnection(); //ket noi
            ps = conn.prepareStatement(query); //nem cau lenh
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //xoa nha phan phoi trai cay
    public void deletedNPP(String nppid) {
        String query = "update NhaPhanPhoi\n"
                + "set Deleted = 1\n"
                + "where ID_NhaPhanPhoi = ?";
        try {
            conn = new DBContext().getConnection(); //ket noi
            ps = conn.prepareStatement(query); //nem cau lenh
            ps.setString(1, nppid);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
}
