/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import entity.Account;
import entity.Bill;
import entity.Cart;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author nhóm 5
 */
public class CustomerDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    //lấy danh sách khách hàng
    public List<Account> getAllCustomer() {
        List<Account> list = new ArrayList<>();

        try {
            String query = "select * from ThongTinNguoiDung where isAdmin = 0";
            conn = new DBContext().getConnection(); //ket noi
            ps = conn.prepareStatement(query); //nem cau lenh
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Account(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //lấy chi tiết sản phẩm mua
    public List<Cart> getAllBuyBill(String id) {
        List<Cart> list = new ArrayList<>();

        try {
            String query = "select sp.ID_SanPham,sp.AnhSanPham,sp.TenSanPham, ct.[SoLuongBan(KG)],ct.[GiaBan(VND)],hd.NgayLapHoaDon\n"
                    + "from  SanPham sp, ChiTietBan ct, HoaDonBan hd, ThongTinNguoiDung tt\n"
                    + "where sp.ID_SanPham = ct.ID_SanPham\n"
                    + "and ct.ID_HoaDonBan = hd.ID_HoaDonBan\n"
                    + "and hd.ID_TaiKhoanKH = tt.ID_TaiKhoan\n"
                    + "and tt.ID_TaiKhoan = ?\n"
                    + "and hd.TrangThai = 1\n"
                    + "order by hd.ID_HoaDonBan desc";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Cart(rs.getString(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getDate(6)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //update tài khoản
    public void updateAccount(String username, String yourname, String birth, String sdt, String address) {
        String query = "UPDATE ThongTinNguoiDung set  Ten = ?, NamSinh = ?, SDT = ?, DiaChi = ? \n"
                + "where TaiKhoan = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, yourname);
            ps.setString(2, birth);
            ps.setString(3, sdt);
            ps.setString(4, address);
            ps.setString(5, username);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //Cập nhật mật khẩu thông qua taikhoan
    public void updateAccountPassword(String username, String password) {
        String query = "update ThongTinNguoiDung set MatKhau = ? where TaiKhoan = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, MD5Encryption(password));
            ps.setString(2, username);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //Mã hóa mật khẩu bắng md5
    public String MD5Encryption(String password) {//Phương thức mã hóa password thành mã băm
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");//Phương thức getInstance tĩnh được gọi với tên MD5
            md.update(password.getBytes());//Sử dụng hàm update để lấy byte của password
            return DatatypeConverter.printHexBinary(md.digest()).toLowerCase();//cuối cùng chúng ta cần sử dụng hàm digest() để tạo mã băm:
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();//In ra lỗi, nếu có
        }
        return null;
    }
    
    //lay thong tin khách hàng theo tên
    public Account getCustomerByName(String cname) {
        String query = "select * from ThongTinNguoiDung where TaiKhoan like ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, cname);
            rs = ps.executeQuery();

            while (rs.next()) {
                return new Account(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getInt(8));
            }
        } catch (Exception e) {
        }

        return null;
    }
}
