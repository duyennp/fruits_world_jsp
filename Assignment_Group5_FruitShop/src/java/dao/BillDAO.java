/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import entity.Bill;
import entity.Distributors;
import entity.Fruit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author nhóm 5
 */
public class BillDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    // lấy chi tiết sản phâm mua
    public List<Bill> getAllBuyBill() {
        List<Bill> list = new ArrayList<>();

        try {
            String query = "SELECT sp.AnhSanPham,sp.TenSanPham,ct.[SoLuongMua(KG)],ct.[GiaMua(VND)],npp.TenNhaPhanPhoi,hd.NgayLapHoaDon \n"
                    + "From SanPham sp, ChiTietMua ct, HoaDonMua hd, NhaPhanPhoi npp\n"
                    + "where sp.ID_SanPham = ct.ID_SanPham\n"
                    + "and ct.ID_HoaDonMua = hd.ID_HoaDonMua\n"
                    + "and hd.ID_NhaPhanPhoi = npp.ID_NhaPhanPhoi\n"
                    + "order by hd.ID_HoaDonMua desc";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Bill(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getString(5), rs.getDate(6)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //lấy chi tiết sản phẩm bán
    public List<Bill> getAllSellBill() {
        List<Bill> list = new ArrayList<>();

        try {
            String query = "select sp.AnhSanPham, sp.TenSanPham, ct.[SoLuongBan(KG)],ct.[GiaBan(VND)], tt.TaiKhoan, hd.NgayLapHoaDon \n"
                    + "From SanPham sp, ChiTietBan ct, HoaDonBan hd, ThongTinNguoiDung tt\n"
                    + "where sp.ID_SanPham = ct.ID_SanPham\n"
                    + "and ct.ID_HoaDonBan = hd.ID_HoaDonBan\n"
                    + "and hd.ID_TaiKhoanKH = tt.ID_TaiKhoan\n"
                    + "and hd.TrangThai = 1\n"
                    + "order by hd.ID_HoaDonBan desc";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Bill(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getString(5), rs.getDate(6)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //xác nhận trang thái hóa đơn sang 1 và update vào id của tk admin
    public void confirmBillSell(String stt, String idAdmin) {
        String queryTong = "select ID_HoaDonBan from HoaDonBan where TrangThai = 0";
        List<Integer> sumHDB = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(queryTong);
            rs = ps.executeQuery();
            while (rs.next()) {
                sumHDB.add(rs.getInt(1));
            }
        } catch (Exception e) {
        }

        int idHDB = sumHDB.get(Integer.parseInt(stt));

        String query = "update HoaDonBan\n"
                + "set TrangThai = 1,\n"
                + "ID_TaiKhoanAdmin = " + idAdmin + " \n"
                + "where ID_HoaDonBan = " + idHDB;
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //huy dat hang
    public void disConfirmBillSell(String stt) {
        String queryTong = "select ID_HoaDonBan from HoaDonBan where TrangThai = 0";
        List<Integer> sumHDB = new ArrayList<>();
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(queryTong);
            rs = ps.executeQuery();
            while (rs.next()) {
                sumHDB.add(rs.getInt(1));
            }
        } catch (Exception e) {
        }

        int idHDB = sumHDB.get(Integer.parseInt(stt));

        String query = "delete from ChiTietBan\n"
                + "where ID_HoaDonBan = "+idHDB+"\n"
                + "delete from HoaDonBan\n"
                + "where ID_HoaDonBan = "+idHDB;
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
}
