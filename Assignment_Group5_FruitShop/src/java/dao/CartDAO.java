/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.DBContext;
import entity.Cart;
import entity.KhoHang;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author nhóm 5
 */
public class CartDAO {

    Connection conn = null; // ket noi sql
    PreparedStatement ps = null; // nem lenh vao sql
    ResultSet rs = null; //nhan ket qua tra ve tu sql

    //lấy list cart
    public List<Cart> getAllCart(String id) {
        List<Cart> list = new ArrayList<>();

        try {
            String query = "select sp.ID_SanPham, sp.AnhSanPham,sp.TenSanPham,gh.[SoLuongTrongGioHang(KG)],kh.[GiaBan(VND)] from SanPham sp, GioHang gh, KhoHang kh, ThongTinNguoiDung tt\n"
                    + "where tt.ID_TaiKhoan = gh.ID_TaiKhoan\n"
                    + "and gh.ID_SanPham = kh.ID_SanPham\n"
                    + "and kh.ID_SanPham = sp.ID_SanPham\n"
                    + "and sp.Deleted = 0 \n"
                    + "and tt.ID_TaiKhoan = ?";
            conn = new DBContext().getConnection(); //ket noi
            ps = conn.prepareStatement(query); //nem cau lenh
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Cart(rs.getString(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //xóa dữ liệu sản phẩm trong giỏ hàng 
    public void deleteCart(String id_tk, String id_sp) {
        String query = "delete from GioHang where ID_TaiKhoan = ? and ID_SanPham = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, id_tk);
            ps.setString(2, id_sp);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    // thanh toán để chuyển từ giỏ hàng sang lịch sử bán hàng
    public void PayCart(String AccountID, List<Cart> list) {
        String query = "INSERT INTO HoaDonBan (ID_TaiKhoanKH,TrangThai) VALUES (" + AccountID + ", " + 0 + ")";

        for (Cart cart : list) {
            query += "insert into ChiTietBan values ((select top (1) ID_HoaDonBan from HoaDonBan order by ID_HoaDonBan desc),"
                    + "" + cart.getId() + "," + cart.getQuantity() + "," + cart.getPrice() + ")";
            deleteCart(AccountID, cart.getId());
        }
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    //check số lượng trái cây còn lại
    public List<KhoHang> checkQuantity() {
        List<KhoHang> list = new ArrayList<>();

        try {
            String query = "select ID_SanPham, [SoLuongTonKho(KG)] from KhoHang";
            conn = new DBContext().getConnection(); //ket noi
            ps = conn.prepareStatement(query); //nem cau lenh
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new KhoHang(rs.getString(1), rs.getInt(2)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //update số lượng trái cây
    public void updateQuantity(int quantity, String id) {
        String query = "update KhoHang set [SoLuongTonKho(KG)] = [SoLuongTonKho(KG)] - ? \n"
                + "where ID_SanPham = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, quantity);
            ps.setString(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }

    }
}
