package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Page_005fSignUp_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("    <!-- Basic -->\n");
      out.write("\n");
      out.write("    <head>\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("\n");
      out.write("        <!-- Mobile Metas -->\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("\n");
      out.write("        <!-- Site Metas -->\n");
      out.write("        <title>Fruits Shop - Tạo Trái Cây</title>\n");
      out.write("        <meta name=\"keywords\" content=\"\">\n");
      out.write("        <meta name=\"description\" content=\"\">\n");
      out.write("        <meta name=\"author\" content=\"\">\n");
      out.write("\n");
      out.write("        <!-- Site Icons -->\n");
      out.write("        <link rel=\"shortcut icon\" href=\"images/Icon.jpg\" type=\"image/x-icon\">\n");
      out.write("        <link rel=\"apple-touch-icon\" href=\"images/apple-touch-icon.png\">\n");
      out.write("\n");
      out.write("        <!-- Bootstrap CSS -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("        <!-- Site CSS -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/style.css\">\n");
      out.write("        <script src=\"js/myScript.js\"></script>\n");
      out.write("    </head>\n");
      out.write("\n");
      out.write("    <body>\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Header.jsp", out, false);
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            <!-- Start All Title Box -->\n");
      out.write("            <div class=\"all-title-box\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-lg-12\">\n");
      out.write("                            <h2>Đăng kí</h2>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <!-- End All Title Box -->\n");
      out.write("\n");
      out.write("            <div class=\"center1\">\n");
      out.write("                <h1 class=\"Login1\">Đăng Kí</h1>\n");
      out.write("                <form action=\"Account_SignUp\" method=\"post\">\n");
      out.write("                    <p style=\"font-size: 25px; color: red\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${Pass}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</p>\n");
      out.write("                <p style=\"font-size: 25px; color: red\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${User}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</p>\n");
      out.write("                <div class=\"txt_field\">\n");
      out.write("                    <input name=\"username\" type=\"text\" required>\n");
      out.write("\n");
      out.write("                    <label>Tài Khoản</label>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"txt_field\">\n");
      out.write("                    <input  id=\"password\" name=\"password\" type=\"password\" required>\n");
      out.write("\n");
      out.write("                    <label>Mật Khảu</label>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"txt_field\">\n");
      out.write("                    <input id=\"repassword\" name=\"repassword\" type=\"password\" required>\n");
      out.write("                    <label>Nhập Lại Mật Khảu</label>\n");
      out.write("                </div>\n");
      out.write("                <div> <input type=\"checkbox\" onclick=\"showPass()\">Hiện Mật Khẩu</div>\n");
      out.write("                <div class=\"txt_field\">\n");
      out.write("                    <input name=\"yourname\" type=\"text\" required>\n");
      out.write("\n");
      out.write("                    <label>Họ Và Tên</label>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"txt_field\">\n");
      out.write("                    <input name=\"birth\" type=\"text\" required>\n");
      out.write("\n");
      out.write("                    <label>Năm Sinh</label>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"txt_field\">\n");
      out.write("                    <input name=\"sdt\" type=\"text\" required>\n");
      out.write("\n");
      out.write("                    <label>Số Điện Thoại</label>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"txt_field\">\n");
      out.write("                    <input name=\"address\" type=\"text\" required>\n");
      out.write("\n");
      out.write("                    <label>Địa Chỉ</label>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"signup_link\">\n");
      out.write("                    Bạn là thành viên? <a href=\"Page_Login.jsp\">Đăng Nhập</a>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <input type=\"submit\" value=\"Đăng Kí\">\n");
      out.write("\n");
      out.write("            </form>\n");
      out.write("        </div>\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Footer.jsp", out, false);
      out.write("\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
