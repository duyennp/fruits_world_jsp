package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items_end;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items_end = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items_end.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("    <!-- Basic -->\n");
      out.write("    <head>\n");
      out.write("        ");
      dao.FruitsDAO a = null;
      synchronized (request) {
        a = (dao.FruitsDAO) _jspx_page_context.getAttribute("a", PageContext.REQUEST_SCOPE);
        if (a == null){
          a = new dao.FruitsDAO();
          _jspx_page_context.setAttribute("a", a, PageContext.REQUEST_SCOPE);
        }
      }
      out.write("\n");
      out.write("            <meta charset=\"utf-8\">\n");
      out.write("            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("\n");
      out.write("            <!-- Mobile Metas -->\n");
      out.write("            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("\n");
      out.write("            <!-- Site Metas -->\n");
      out.write("            <title>Fruits Shop</title>\n");
      out.write("            <meta name=\"keywords\" content=\"\">\n");
      out.write("            <meta name=\"description\" content=\"\">\n");
      out.write("            <meta name=\"author\" content=\"\">\n");
      out.write("\n");
      out.write("            <!-- Site Icons -->\n");
      out.write("            <link rel=\"shortcut icon\" href=\"images/Icon.jpg\" type=\"image/x-icon\">\n");
      out.write("            <link rel=\"apple-touch-icon\" href=\"images/apple-touch-icon.png\">\n");
      out.write("\n");
      out.write("            <!-- Bootstrap CSS -->\n");
      out.write("            <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("            <!-- Site CSS -->\n");
      out.write("            <link rel=\"stylesheet\" href=\"css/style.css\">\n");
      out.write("\n");
      out.write("\n");
      out.write("        </head>\n");
      out.write("\n");
      out.write("        <body>\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Header.jsp", out, false);
      out.write("\n");
      out.write("\n");
      out.write("            <!-- Start Slider -->\n");
      out.write("            <div class=\"carousel-inner\">\n");
      out.write("                <div class=\"item active\">\n");
      out.write("                    <img src=\"images/anh6.PNG\" alt=\"banner\">\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <!-- End Slider -->\n");
      out.write("\n");
      out.write("            <!-- Start Categories  -->\n");
      out.write("            <div class=\"categories-shop\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">\n");
      out.write("                            <div class=\"shop-cat-box\">\n");
      out.write("                                <img class=\"img-fluid\" src=\"images/mientrung.png\" alt=\"Sản Phẩm Miền Trung\" />\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">\n");
      out.write("                            <div class=\"shop-cat-box\">\n");
      out.write("                                <img class=\"img-fluid\" src=\"images/bac.png\" alt=\"Sản Phẩm Miền Bắc\" />\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">\n");
      out.write("                            <div class=\"shop-cat-box\">\n");
      out.write("                                <img class=\"img-fluid\" src=\"images/miennam.png\" alt=\"Sản Phẩm Miền Nam\" />\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <!-- End Categories -->\n");
      out.write("\n");
      out.write("            <div class=\"box-add-products\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                            <div class=\"offer-box-product\">\n");
      out.write("                                <img style=\"width: 85%; height: 90%; padding-left: 20%;\" class=\"img-fluid\" src=\"images/sale20.jpg\" alt=\"Ảnh Sale\" />\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                            <div class=\"offer-box-product\">\n");
      out.write("                                <img style=\"width: 85%; height: 90%;padding-right: 20%;\" class=\"img-fluid\" src=\"images/sale50.jpg\" alt=\"Ảnh Sale\" />\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <!-- Start Products  -->\n");
      out.write("            <div class=\"products-box\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-lg-12\">\n");
      out.write("                            <div class=\"title-all text-center\">\n");
      out.write("                                <h1>Trái Cây Mới Nhất</h1>\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"row special-list\">\n");
      out.write("                    ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- End Products  -->\n");
      out.write("\n");
      out.write("        <!-- Start Blog  -->\n");
      out.write("        <div class=\"latest-blog\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-lg-12\">\n");
      out.write("                        <div class=\"title-all text-center\">\n");
      out.write("                            <h1>Trái Cây Sắp Ra Mắt</h1>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-6 col-lg-4 col-xl-4\">\n");
      out.write("                        <div class=\"blog-box\">\n");
      out.write("                            <div class=\"blog-img\">\n");
      out.write("                                <img class=\"img-fluid\" src=\"images/xoai.jpg\" alt=\"Ảnh Sản Phẩm\" />\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"blog-content\">\n");
      out.write("                                <div class=\"title-blog\">\n");
      out.write("                                    <h3>Xoài Bắc Úc</h3>\n");
      out.write("                                    <p>Xoài vùng Bắc Úc (Northern Territory Mango) là một loại trái cây có giá trị kinh tế rất cao. Vào năm 2012, một khay xoài 12 quả đã được bán với mức giá kỷ lục là 50.000 USD (tương đương khoảng 1,04 tỷ đồng).</p>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-md-6 col-lg-4 col-xl-4\">\n");
      out.write("                        <div class=\"blog-box\">\n");
      out.write("                            <div class=\"blog-img\">\n");
      out.write("                                <img class=\"img-fluid\" src=\"images/cam.jpg\" alt=\"Ảnh Sản Phẩm\" />\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"blog-content\">\n");
      out.write("                                <div class=\"title-blog\">\n");
      out.write("                                    <h3>Cam DEKOPON</h3>\n");
      out.write("                                    <p>Cam Dekopon (hay còn gọi Cam Sumo) là một loại trái cây lai tạo giữa cam và quýt từ năm 1972 ở Nhật Bản. Loại cam lai này nổi tiếng về độ mọng của màng ngăn giữa các múi và có chất lượng tuyệt vời</p>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-md-6 col-lg-4 col-xl-4\">\n");
      out.write("                        <div class=\"blog-box\">\n");
      out.write("                            <div class=\"blog-img\">\n");
      out.write("                                <img class=\"img-fluid\" src=\"images/dua.jpg\" alt=\"Ảnh Sản Phẩm\" />\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"blog-content\">\n");
      out.write("                                <div class=\"title-blog\">\n");
      out.write("                                    <h3>Dưa Yubari King</h3>\n");
      out.write("                                    <p>Yubari King là một giống dưa lưới quý hiếm được liệt vào danh sách những giống thực vật cần được bảo tồn tại Nhật Bản. Thời kì phong kiến Nhật Bản, chỉ có các vua chúa hay người có địa vị cao trong xã hội mới được thưởng thức món dưa thơm ngon này.</p>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <!-- End Blog  -->\n");
      out.write("\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Footer.jsp", out, false);
      out.write("\n");
      out.write("    </body>\n");
      out.write("\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items_end.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${a.inforAllFruits}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("j");
    _jspx_th_c_forEach_0.setEnd(3);
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                        <div class=\"col-lg-3 col-md-6 special-grid best-seller\">\n");
          out.write("                            <div class=\"products-single fix\">\n");
          out.write("                                <div class=\"box-img-hover\">\n");
          out.write("                                    <div class=\"type-lb\">\n");
          out.write("                                        <p class=\"sale\">Sale 20%</p>\n");
          out.write("                                    </div>\n");
          out.write("                                    <img src=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${j.photo}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\" class=\"img-fluid\" alt=\"Ảnh Sản Phẩm\">\n");
          out.write("                                    <div class=\"mask-icon\">\n");
          out.write("                                        <a class=\"cart\" href=\"Fruit_Choose?fid=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${j.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">Thêm vào giỏ hàng</a>\n");
          out.write("                                    </div>\n");
          out.write("                                </div>\n");
          out.write("                                <div class=\"why-text\">\n");
          out.write("                                    <h4>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${j.name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</h4>\n");
          out.write("                                    <h4 style=\"color: red\"><del>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${j.price + j.price*0.25}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write(" VNĐ</del></h4>\n");
          out.write("                                    <h5>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${j.price}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write(" VNĐ</h5>\n");
          out.write("                                    <div style=\"margin-left: -8px; margin-right: 8px\" id=\"inforFruitMobi\" class=\"add-pr\">\n");
          out.write("                                        <a style=\"margin-top: 4px; display: block; width: 100%\"  class=\"btn hvr-hover\" href=\"Fruit_Choose?fid=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${j.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">Thêm Trái Cây</a>\n");
          out.write("                                    </div>\n");
          out.write("                                </div>\n");
          out.write("                            </div>\n");
          out.write("                        </div>\n");
          out.write("                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items_end.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }
}
