<%-- 
    Document   : Admin_CreateFruits
    Created on : Oct 25, 2021, 8:21:15 PM
    Author     : nhóm 5
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Tạo Trái Cây</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">

    </head>

    <body>
        <!-- Tạo trái cây mới cho shop -->
        <jsp:include page="Error.jsp"></jsp:include>
        <c:if test="${sessionScope.acc.isAdmin == 1}">

            <jsp:include page="Header.jsp"></jsp:include>


                <!-- Start All Title Box -->
                <div class="all-title-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Tạo Trái Cây Mới </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End All Title Box -->

                <div class="center1">
                    <h1 class="Login1">Trái cây</h1>
                    <form action="Admin_CreateFruit" method="post">
                        <div class="txt_field">
                            <input name="nameFruit" type="text" required>
                            <label>Tên</label>
                        </div>
                        <div class="txt_field">
                            <input name="photoFruit" type="text" required>
                            <label>Link Ảnh Sản Phẩm</label>
                        </div>
                        <div class="txt_field">
                            <input name="inforFruit" type="text" required>
                            <label>Thông Tin</label>
                        </div>
                        <div class="txt_field">
                            <input name="priceFruit" type="number" min="1000" required>
                            <label>Giá Tiền ( VNĐ )</label>
                        </div>
                        <input type="submit" value="Xác Nhận">
                        <div>
                            <h3><a href="Admin_Manager.jsp">Quay về</a></h3>
                        </div>
                    </form>
                </div>


            <jsp:include page="Footer.jsp"></jsp:include>
        </c:if>
    </body>

</html>

