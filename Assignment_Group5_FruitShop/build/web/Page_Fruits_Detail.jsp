<%-- 
    Document   : Fruits_Detail
    Created on : Oct 24, 2021, 6:23:33 AM
    Author     : nhóm 5
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Thông Tin Sản Phẩm</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">
        <jsp:useBean id="a" class="dao.FruitsDAO" scope="request"></jsp:useBean>
    </head>

    <body>
        <!-- Hiển thị ra thông tin trái cây được chọn -->
        <jsp:include page="Header.jsp"></jsp:include>

            <!-- Start All Title Box -->
            <div class="all-title-box">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Thông Tin Sản Phẩm</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End All Title Box -->

            <!-- Start Shop Detail  -->
            <div class="shop-detail-box-main">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-5 col-lg-5 col-md-6">
                            <div id="carousel-example-1" class="single-product-slider carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active"> <img class="d-block w-100" src="${InforFruit.photo}" alt="First slide"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-6">
                        <div class="single-product-details">
                            <h2>Tên sản phẩm: ${InforFruit.name}</h2>
                            <h4>Giá: </h4>
                            <h5><del  style="color: red">${InforFruit.price + InforFruit.price*0.25}  VNĐ</del> ${InforFruit.price} VNĐ</h5>
                            <p class="available-stock"><span> Số lượng còn ${InforFruit.quantity} kg trong kho/ <a href="#">${quantitySell} kg đã bán</a></span><p>  
                            <h4>Mô tả sản phẩm:</h4>
                            <p>${InforFruit.infor}</p>
                            <form action="Cart_Add?fid=${InforFruit.id}" method="post">
                                <ul>
                                    <li>
                                        <div class="form-group quantity-box">
                                            <label class="control-label">Nhập số lượng mua (kg)</label>
                                            <input id="quantityFruit" name="quantityFruit" value="1" class="form-control" min="1" max="${InforFruit.quantity}" placeholder="Nhập số lượng(KG)" type="number" required>
                                        </div>
                                    </li>
                                </ul>
                                            <h4 style="color: red">${Mess}</h4>
                                <div class="price-box-bar">
                                    <div class="cart-and-bay-btn">								
                                        <!--<a class="btn hvr-hover" data-fancybox-close="" href="#">Thêm Vào Giỏ Hàng </a>-->
                                        <c:if test="${sessionScope.acc != null && InforFruit.quantity > 0}">
                                            <input style="width: 150px" class="btn hvr-hover" type="submit" value="Thêm vào giỏ hàng" >
                                        </c:if>
                                            <c:if test="${sessionScope.acc != null && InforFruit.quantity <= 0}">
                                            <h2 style="color: red;width: "> Hết Hàng</h2>
                                        </c:if>
                                        <c:if test="${sessionScope.acc == null}">   
                                            <a class="btn hvr-hover" data-fancybox-close="" href="Page_Login.jsp">Đăng nhập để mua</a>
                                        </c:if>
                                        <a class="btn hvr-hover" data-fancybox-close="" href="Page_Shop.jsp">Quay Về Shop</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Cart -->

        <jsp:include page="Footer.jsp"></jsp:include>
    </body>

</html>