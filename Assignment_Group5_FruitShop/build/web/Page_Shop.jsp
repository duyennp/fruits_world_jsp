<%-- 
    Document   : Shop
    Created on : Oct 24, 2021, 6:07:27 AM
    Author     : nhóm 5
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <jsp:useBean id="a" class="dao.FruitsDAO" scope="request"></jsp:useBean>

            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <!-- Mobile Metas -->
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <!-- Site Metas -->
            <title>Fruits Shop - Danh Sách Trái Cây</title>
            <meta name="keywords" content="">
            <meta name="description" content="">
            <meta name="author" content="">

            <!-- Site Icons -->
            <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
            <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <!-- Site CSS -->
            <link rel="stylesheet" href="css/style.css">
            <link rel="stylesheet" href="css/custom.css">



        </head>
        <body>
            <!-- Hiển thị ra toàn bộ trái cây -->
        <jsp:include page="Header.jsp"></jsp:include>
            <!-- Start All Title Box -->
            <div class="all-title-box">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Sản Phẩm</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End All Title Box -->

            <!-- Start Shop Page  -->
            <div class="shop-box-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-9 col-lg-9 col-sm-12 col-xs-12 shop-content-right">
                            <div class="right-product-box">
                                <div class="product-item-filter row">
                                    <div style="font-weight: bold;margin-left: -100px" class="col-12 col-sm-8 text-center text-sm-left">
                                    <c:if test="${listF == null}">
                                        <p>Có ${a.inforAllFruits.size()} kết quả</p>
                                    </c:if>
                                    <c:if test="${listF != null}">
                                        <p>Có ${listF.size()} kết quả</p>
                                    </c:if>
                                </div>
                            </div>

                            <div class="product-categorie-box">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade show active" id="grid-view">
                                        <div class="row">
                                            <c:if test="${listF == null}">
                                                <c:forEach items="${a.inforAllFruits}" var="j" end="8">
                                                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                        <div class="products-single fix">
                                                            <div class="box-img-hover">
                                                                <div class="type-lb">
                                                                    <p class="sale">Sale 20%</p>
                                                                </div>
                                                                <img src="${j.photo}" class="img-fluid" alt="Ảnh Sản Phẩm">
                                                                <div class="mask-icon">
                                                                    <a class="cart" href="Fruit_Choose?fid=${j.id}">Xem Thông Tin</a>
                                                                </div>
                                                            </div>
                                                            <div class="why-text">
                                                                <h4>${j.name}</h4>
                                                                <h4 style="color: red"><del>${j.price + j.price*0.25} VNĐ</del></h4>
                                                                <h5>${j.price} VNĐ</h5>
                                                                <div style="margin-left: -8px; margin-right: 8px" id="inforFruitMobi" class="add-pr">
                                                                    <a style="margin-top: 4px; display: block; width: 100%"  class="btn hvr-hover" href="Fruit_Choose?fid=${j.id}">Thêm Trái Cây</a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </c:if>

                                            <c:forEach items="${listF}" var="j">
                                                <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                                    <div class="products-single fix">
                                                        <div class="box-img-hover">
                                                            <div class="type-lb">
                                                                <p class="sale">Sale 20%</p>
                                                            </div>
                                                            <img src="${j.photo}" class="img-fluid" alt="Ảnh Sản Phẩm">
                                                            <div class="mask-icon">
                                                                <a class="cart" href="Fruit_Choose?fid=${j.id}">Xem Thông Tin</a>
                                                            </div>
                                                        </div>
                                                        <div class="why-text">
                                                            <h4>${j.name}</h4>
                                                            <h4 style="color: red"><del>${j.price + j.price*0.25} VNĐ</del></h4>
                                                            <h5>${j.price} VNĐ</h5>
                                                            <div style="margin-left: -8px; margin-right: 8px" id="inforFruitMobi" class="add-pr">
                                                                <a style="margin-top: 4px; display: block; width: 100%"  class="btn hvr-hover" href="Fruit_Choose?fid=${j.id}">Thêm Trái Cây</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="flexCenterRow">
                                <c:forEach begin="1" end="5" var="x">
                                    <a href="Fruit_PhanTrang?pt=${x}" style="padding: 5px; margin: 0 10px">${x}</a>
                                </c:forEach>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-sm-12 col-xs-12 sidebar-shop-left">
                        <div class="product-categori">
                            <div class="search-product">
                                <form action="Fruit_Search">
                                    <input class="form-control" name="searchF" placeholder="Tiềm Kiếm..." type="text">
                                    <button type="submit"> <i class="fa fa-search"></i> </button>
                                </form>
                            </div>
                            <div class="filter-sidebar-left">
                                <div class="title-left">
                                    <h3>Phân Loại</h3>
                                </div>
                                <div class="list-group list-group-collapse list-group-sm list-group-tree" id="list-group-men" data-children=".sub-men">
                                    <div style="font-weight: bold" class="list-group-collapse sub-men">

                                        <p>Giá Tiền</p>
                                        <div class="collapse show" id="sub-men1" data-parent="#list-group-men">
                                            <div class="list-group">
                                                <c:forEach items="${a.allClassify}" var="pl">
                                                    <a href="Fruit_Type?did=${pl.id}" >${pl.name} VNĐ</a>
                                                </c:forEach> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Shop Page -->


        <jsp:include page="Footer.jsp"></jsp:include>
    </body>

</html>
