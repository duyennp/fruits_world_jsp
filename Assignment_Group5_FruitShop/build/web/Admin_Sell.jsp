<%-- 
    Document   : Admin_Sell
    Created on : Oct 24, 2021, 6:26:36 AM
    Author     : nhóm 5
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Lịch Sử Bán Hàng</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">
        <jsp:useBean id="a" class="dao.BillDAO" scope="request" ></jsp:useBean>
        </head>

        <body>
            <!-- Hiện thị các hóa đơn đã bán cho khách hàng -->
        <jsp:include page="Error.jsp"></jsp:include>
        <c:if test="${sessionScope.acc.isAdmin == 1}">
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- Start All Title Box -->
                <div class="all-title-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Lịch Sử Bán Hàng</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End All Title Box -->
            <jsp:include page="Admin_Option.jsp"></jsp:include>

                <!-- Start Wishlist  -->
                <div class="wishlist-box-main">
                    <div class="inforNhaPhanPhoi">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-main table-responsive">
                                    <table style="font-weight: bold" class="table">
                                        <thead>
                                            <tr>
                                                <th>Sản phẩm</th>
                                                <th>Tên</th>
                                                <th>Số Lượng ( KG )</th>
                                                <th>Giá ( VNĐ )</th>
                                                <th>Khách Hàng</th>
                                                <th>Ngày Mua</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach items="${a.allSellBill}" var="x">
                                            <tr style="font-weight: bold; color: black">
                                                <td class="thumbnail-img">
                                                    <img class="img-fluid" src="${x.image}" alt="Ảnh Sản Phẩm"/>
                                                </td>
                                                <td class="name-pr">
                                                    ${x.name}
                                                </td>
                                                <td class="price-pr">
                                                    <p>${x.price}</p>
                                                </td>
                                                <td class="quantity-box">${x.quantity} </td>
                                                <td class="quantity-box">${x.name_person} </td>
                                                <td class="quantity-box">${x.date} </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Wishlist -->
            <jsp:include page="Footer.jsp"></jsp:include>
        </c:if>
    </body>
</html>
