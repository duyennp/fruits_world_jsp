<%-- 
    Document   : index
    Created on : Oct 24, 2021, 6:04:15 AM
    Author     : nhóm 5
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->
    <head>
        <jsp:useBean id="a" class="dao.FruitsDAO" scope="request"></jsp:useBean>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <!-- Mobile Metas -->
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <!-- Site Metas -->
            <title>Fruits Shop</title>
            <meta name="keywords" content="">
            <meta name="description" content="">
            <meta name="author" content="">

            <!-- Site Icons -->
            <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
            <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <!-- Site CSS -->
            <link rel="stylesheet" href="css/style.css">


        </head>

        <body>
            <!-- Trang chủ -->
        <jsp:include page="Header.jsp"></jsp:include>

            <!-- Start Slider -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="images/anh6.PNG" alt="banner">
                </div>
            </div>

            <!-- End Slider -->

            <!-- Start Categories  -->
            <div class="categories-shop">
                <div class="container">

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="shop-cat-box">
                                <img class="img-fluid" src="images/mientrung.png" alt="Sản Phẩm Miền Trung" />

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="shop-cat-box">
                                <img class="img-fluid" src="images/bac.png" alt="Sản Phẩm Miền Bắc" />

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="shop-cat-box">
                                <img class="img-fluid" src="images/miennam.png" alt="Sản Phẩm Miền Nam" />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Categories -->

            <div class="box-add-products">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="offer-box-product">
                                <img style="width: 85%; height: 90%; padding-left: 20%;" class="img-fluid" src="images/sale20.jpg" alt="Ảnh Sale" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="offer-box-product">
                                <img style="width: 85%; height: 90%;padding-right: 20%;" class="img-fluid" src="images/sale50.jpg" alt="Ảnh Sale" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Start Products  -->
            <div class="products-box">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="title-all text-center">
                                <h1>Trái Cây Mới Nhất</h1>

                            </div>
                        </div>
                    </div>

                    <div class="row special-list">
                    <c:forEach items="${a.inforAllFruits}" var="j" end="3" >
                        <div class="col-lg-3 col-md-6 special-grid best-seller">
                            <div class="products-single fix">
                                <div class="box-img-hover">
                                    <div class="type-lb">
                                        <p class="sale">Sale 20%</p>
                                    </div>
                                    <img src="${j.photo}" class="img-fluid" alt="Ảnh Sản Phẩm">
                                    <div class="mask-icon">
                                        <a class="cart" href="Fruit_Choose?fid=${j.id}">Thêm vào giỏ hàng</a>
                                    </div>
                                </div>
                                <div class="why-text">
                                    <h4>${j.name}</h4>
                                    <h4 style="color: red"><del>${j.price + j.price*0.25} VNĐ</del></h4>
                                    <h5>${j.price} VNĐ</h5>
                                    <div style="margin-left: -8px; margin-right: 8px" id="inforFruitMobi" class="add-pr">
                                        <a style="margin-top: 4px; display: block; width: 100%"  class="btn hvr-hover" href="Fruit_Choose?fid=${j.id}">Thêm Trái Cây</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <!-- End Products  -->

        <!-- Start Blog  -->
        <div class="latest-blog">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="title-all text-center">
                            <h1>Trái Cây Sắp Ra Mắt</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-4 col-xl-4">
                        <div class="blog-box">
                            <div class="blog-img">
                                <img class="img-fluid" src="images/xoai.jpg" alt="Ảnh Sản Phẩm" />
                            </div>
                            <div class="blog-content">
                                <div class="title-blog">
                                    <h3>Xoài Bắc Úc</h3>
                                    <p>Xoài vùng Bắc Úc (Northern Territory Mango) là một loại trái cây có giá trị kinh tế rất cao. Vào năm 2012, một khay xoài 12 quả đã được bán với mức giá kỷ lục là 50.000 USD (tương đương khoảng 1,04 tỷ đồng).</p>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xl-4">
                        <div class="blog-box">
                            <div class="blog-img">
                                <img class="img-fluid" src="images/cam.jpg" alt="Ảnh Sản Phẩm" />
                            </div>
                            <div class="blog-content">
                                <div class="title-blog">
                                    <h3>Cam DEKOPON</h3>
                                    <p>Cam Dekopon (hay còn gọi Cam Sumo) là một loại trái cây lai tạo giữa cam và quýt từ năm 1972 ở Nhật Bản. Loại cam lai này nổi tiếng về độ mọng của màng ngăn giữa các múi và có chất lượng tuyệt vời</p>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xl-4">
                        <div class="blog-box">
                            <div class="blog-img">
                                <img class="img-fluid" src="images/dua.jpg" alt="Ảnh Sản Phẩm" />
                            </div>
                            <div class="blog-content">
                                <div class="title-blog">
                                    <h3>Dưa Yubari King</h3>
                                    <p>Yubari King là một giống dưa lưới quý hiếm được liệt vào danh sách những giống thực vật cần được bảo tồn tại Nhật Bản. Thời kì phong kiến Nhật Bản, chỉ có các vua chúa hay người có địa vị cao trong xã hội mới được thưởng thức món dưa thơm ngon này.</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Blog  -->

        <jsp:include page="Footer.jsp"></jsp:include>
    </body>

</html>
