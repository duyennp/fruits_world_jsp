<%-- 
    Document   : Admin_CreateFruits
    Created on : Oct 25, 2021, 8:21:15 PM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <jsp:useBean id="a" class="dao.DistributorsDAO" scope="request"></jsp:useBean>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <!-- Mobile Metas -->
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <!-- Site Metas -->
            <title>Fruits Shop - Xác Nhận Số Loại</title>
            <meta name="keywords" content="">
            <meta name="description" content="">
            <meta name="author" content="">

            <!-- Site Icons -->
            <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
            <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <!-- Site CSS -->
            <link rel="stylesheet" href="css/style.css">

        </head>

        <body>
            <!-- Nhập hàng từ nhà phân phối -->
        <jsp:include page="Error.jsp"></jsp:include>
        <c:if test="${sessionScope.acc.isAdmin == 1}">
            <jsp:include page="Header.jsp"></jsp:include>


                <!-- Start All Title Box -->
                <div class="all-title-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Xác Nhận Số Loại</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End All Title Box -->


                <!-- Start Wishlist  -->
                <div class="wishlist-box-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-main table-responsive">
                                    <form action="Admin_ShowDistributor?type=comfirmFruits" >
                                        <div class="flexCenter">
                                            <div class="flexCenterRow" style="font-size: 20px">
                                                <div style="margin-right: 10px;">
                                                    <label for="nameNPP" style="display: block;margin-bottom: 10px;color: black;font-weight: bold" >Tên Nhà Phân Phối:</label><br>
                                                    <label for="quantity" style="color: black;font-weight: bold">Số Lượng Loại Trái Cây: </label>
                                                </div>                                            
                                                <input  type="hidden" value="comfirmFruits" id="type" name="type"><br>
                                                <div>
                                                    <select id="idNPP" name="idNPP"style="display: block;margin-bottom: 44px;min-width: 139px">
                                                    <c:forEach items="${a.allDistributors}" var="x">
                                                        <option value="${x.id}" >${x.name}</option>
                                                    </c:forEach>
                                                </select>
                                                <input type="number" value="1" min="1" id="quantity" name="quantity" placeholder="Nhập Số Loại Trái Cây"><br>
                                            </div>
                                        </div>
                                        <div style="width: 300px;"><input  class="btn hvr-hover" type="submit" value="xacNhan"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Wishlist -->

            <jsp:include page="Footer.jsp"></jsp:include>
        </c:if>
    </body>

</html>

