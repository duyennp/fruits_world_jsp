<%-- 
    Document   : Cart
    Created on : Oct 24, 2021, 6:12:25 AM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Giỏ Hàng</title>
        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">



    </head>

    <body>
        <!-- Thông báo lỗi khi người dùng mua quá lố số trái cây trong kho -->
        <jsp:include page="Header.jsp"></jsp:include>
        <jsp:include page="ErrorNullAccount.jsp"></jsp:include>
        <c:if test="${sessionScope.acc !=null}">
        <!-- Start All Title Box -->
            <div class="all-title-box">
                <div class="container">
                    <div class="row">
                    </div>
                </div>
            </div>
            <!-- End All Title Box -->
            <div class="my-account-box-main">
            <div class="container">
                <div class="my-account-page">
                    <div class="row">
                        <div style="margin-left: auto; margin-right: 230px" class="col-lg-4 col-md-12">
                                <div class="service-box">
                                    <div class="service-icon">
                                        <a  href="Page_Shop.jsp"><i class='fas fa-frown-open' style='font-size:97px;color:yellow;margin-top: -4px; margin-left: -2px'></i> </a>
                                    </div>
                                    <div style="margin-left: -335px; margin-right: auto; width: 300%;">
                                        <h4 style="width: 100%; font-weight: bold; font-size: 22px; color: green">Xin Lỗi Về Sự Bất Tiện Này, Những Sản Phẩm Mà Qúy Khách Chọn Không Còn Đủ Số Lượng: <br><c:forEach items="${sp}" var="x"><p style="color: red">${x.getKey()} <span style="color: black">(Còn ${x.getValue()} KG),</span></p></c:forEach></h4>
                                    </div>
                                    
                                    <div style="margin-left: -43px;margin-top: 30px">
                                        
                                        <button class="btn hvr-hover"><a style="font-weight: bold; font-size: 22px" href="Cart_Show">Quay Về Giỏ Hàng</a></button>
                                    </div>
                                </div>

                        </div>                     
                    </div>
                </div>
            </div>
        </div>
</c:if>
        <jsp:include page="Footer.jsp"></jsp:include>
    </body>

</html>
