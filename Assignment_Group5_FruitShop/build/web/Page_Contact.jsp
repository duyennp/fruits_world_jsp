<%-- 
    Document   : Contact
    Created on : Oct 24, 2021, 6:09:23 AM
    Author     : nhóm 5
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Kết Nối</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">

    </head>

    <body>
        <!-- Biểu mẫu feedback -->
        <jsp:include page="Header.jsp"></jsp:include>

        <!-- Start All Title Box -->
        <div class="all-title-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Gửi Đánh Gía Về Fruits Shop</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- End All Title Box -->

        <!-- Start Contact Us  -->
        <div class="contact-box-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-sm-12">
                        <div class="contact-form-right">
                            <h2>GỬI ĐÁNH GIÁ VỀ FRUITS SHOP</h2>
                            <p>Trong quá trình tham gia, trải nghiệm về Fruits Shop, nếu bạn có gì không hài lòng thì có thể gửi đánh giá cho chúng tôi biết để sửa chữa và làm hài lòng quý khách. Cảm ơn</p>
                            <form id="contactForm">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Tên Của Bạn" required data-error="Please enter your name">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="subject" name="name" placeholder="Chủ Đề" required data-error="Please enter your Subject">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control" id="message" placeholder="Lời Đánh Gía Của Bạn" rows="4" data-error="Write your message" required></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="submit-button text-center">
                                            <button style="width: 150px" class="btn hvr-hover" id="submit" type="submit">GỬI</button>
                                            <div id="msgSubmit" class="h3 text-center hidden"></div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <div class="contact-info-left">
                            <h2>THÔNG TIN LIÊN LẠC</h2>
                            <p>Với tôn chỉ “Mang đến cho khách hàng không chỉ là những sản phẩm trái cây an toàn, chất lượng cao, mà kèm theo đó là những dịch vụ tiện ích thân thiện.”, FruitsShop - đơn vị chuyên nhập khẩu các loại trái cây cao cấp từ các nước trên thế giới đang từng bước phát triển và chiếm được lòng tin của người tiêu dùng Việt Nam.</p>
                            <ul>
                                <li>
                                    <p><i class="fas fa-map-marker-alt"></i>Địa Chỉ: FPT Cần Thơ </p>
                                </li>
                                <li>
                                    <p><i class="fas fa-phone-square"></i>Số Điện Thoại: <a href="tel:0788751149">0788751149</a></p>
                                </li>
                                <li>
                                    <p><i class="fas fa-envelope"></i>Email: <a href="mailto:duyennpce150850@fpt.edu.vn">duyennpce150850@fpt.edu.vn</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Cart -->

        <jsp:include page="Footer.jsp"></jsp:include>
    </body>

</html>
