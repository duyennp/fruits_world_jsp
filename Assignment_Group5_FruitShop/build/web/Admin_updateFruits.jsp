<%-- 
    Document   : Admin_updateFruits
    Created on : Oct 24, 2021, 6:28:27 AM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Cập Nhật Trái Cây</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">

    </head>

    <body>
        <!-- Chĩnh sửa thông tin trái cây -->
        <jsp:include page="Error.jsp"></jsp:include>
        <c:if test="${sessionScope.acc.isAdmin == 1}">
            <jsp:include page="Header.jsp"></jsp:include>

                <!-- Start All Title Box -->
                <div class="all-title-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Chỉnh sửa Sản Phẩm</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End All Title Box -->

                <!-- Start Shop Detail  -->
                <div class="shop-detail-box-main">
                    <div class="container">
                        <div class="row">

                            <div class="col-xl-5 col-lg-5 col-md-6">
                                <div id="carousel-example-1" class="single-product-slider carousel slide" data-ride="carousel">
                                    <div class="carousel-inner" role="listbox">
                                        <div class="carousel-item active"> <img class="d-block w-100" src="${fruit.photo}" alt="First slide"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-6">
                            <div class="single-product-details">
                                <h2>Tên Sản Phẩm: ${fruit.name}</h2>

                                <form action="Admin_UpdateFruit?fid=${fruit.id}" method="post">
                                    <h4>Link ảnh sản phẩm: <input id="photoFruit" name="photoFruit" style="border:solid; width: 100%;" type="text" value="${fruit.photo}"></h4>

                                    <h4>Mô tả sản phẩm:</h4>

                                    <div class="form-group quantity-box">
                                        <input id="motaFruit" name="motaFruit" value="${fruit.infor}" >
                                    </div>
                                    <h5>Giá: <input type="number" min="1000" id="priceFruit" name="priceFruit" value="${fruit.price}"> VNĐ</h5>
                                    <h5>Phân Loại: 
                                        <select name="idpl" id="idpl">
                                            <option value="1" ${fruit.name_Type == "0-50k"?"selected":""}>0-50k</option>
                                            <option value="2" ${fruit.name_Type == "50k-200k"?"selected":""}>50k-200k</option>
                                            <option value="3" ${fruit.name_Type == "200k-500k"?"selected":""}>200k-500k</option>
                                            <option value="4" ${fruit.name_Type == "500k-1000k"?"selected":""}>500k-1000k</option>                                    
                                            <option value="5" ${fruit.name_Type == ">1000k"?"selected":""}>>1000k</option>
                                        </select>
                                    </h5>
                                    <ul>
                                        <li>
                                            <div class="form-group quantity-box">
                                                <label class="control-label">Số Lượng</label>
                                                <input id="quantityFruit" name="quantityFruit" class="form-control" value="${fruit.quantity}" min="0" type="number">
                                            </div>
                                        </li>
                                    </ul>

                                    <div class="price-box-bar">
                                        <div class="cart-and-bay-btn">						
                                            <input style=" width: 100px; margin-right:10px " type="submit" class="btn hvr-hover" value="XacNhan">
                                            <a class="btn hvr-hover" data-fancybox-close="" href="Admin_Manager.jsp">Quay Lại</a>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End Cart -->
            <jsp:include page="Footer.jsp"></jsp:include>
        </c:if>
    </body>

</html>
