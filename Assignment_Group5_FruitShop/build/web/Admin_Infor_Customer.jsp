<%-- 
    Document   : Admin_Infor_Customer
    Created on : Oct 26, 2021, 9:17:02 AM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="a" class="dao.CustomerDAO" scope="request"></jsp:useBean>
    <!DOCTYPE html>
    <html lang="en">
        <!-- Basic -->

        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <!-- Mobile Metas -->
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <!-- Site Metas -->
            <title>Fruits Shop - Danh Sách Khách Hàng</title>
            <meta name="keywords" content="">
            <meta name="description" content="">
            <meta name="author" content="">

            <!-- Site Icons -->
            <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
            <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <!-- Site CSS -->
            <link rel="stylesheet" href="css/style.css">

        </head>

        <body>
            <!-- Hiển thị danh sách khách hàng -->
        <jsp:include page="Error.jsp"></jsp:include>
        <c:if test="${sessionScope.acc.isAdmin == 1}">
            <jsp:include page="Header.jsp"></jsp:include>


                <!-- Start All Title Box -->
                <div class="all-title-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Danh Sách Khách Hàng</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End All Title Box -->
            <jsp:include page="Admin_Option.jsp"></jsp:include>

                <div class="cart-box-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-main table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Tên Tài Khoản</th>
                                                <th>Tên Khách Hàng</th>
                                                <th>Năm Sinh</th>
                                                <th>SDT</th>
                                                <th>Địa Chỉ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach items="${a.allCustomer}" var="x">
                                            <tr  style="font-weight: bold; color: black">
                                                <td class="name-pr">
                                                    <p>
                                                        ${x.username}
                                                    </p>
                                                </td>
                                                <td>
                                                    <p>${x.yourname}</p>
                                                </td>
                                                <td>
                                                    <p>${x.birth}</p>
                                                </td>
                                                <td>
                                                    <p>${x.sdt}</p>
                                                </td>
                                                <td>
                                                    <p>${x.address}</p>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Wishlist -->

            <jsp:include page="Footer.jsp"></jsp:include>
        </c:if>
    </body>

</html>