<%-- 
    Document   : Admin_UpdateNPP
    Created on : Nov 2, 2021, 9:34:45 AM
    Author     : nhóm 5
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Chỉnh Sử thông tin Khách Hàng</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">
        <script src="js/myScript.js"></script>
    </head>

    <body>
        <!-- Cập nhật mới các nhà phân phối -->
        <jsp:include page="Error.jsp"></jsp:include>
        <c:if test="${sessionScope.acc.isAdmin == 1}">
            <jsp:include page="Header.jsp"></jsp:include>

                <!-- Start All Title Box -->
                <div class="all-title-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Chỉnh Sửa Thông Tin Nhà Phân Phối</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End All Title Box -->

                <div class="center1">
                    <h1 >Thay Đổi Thông Tin Nhà Phân Phối</h1>
                    <form class="form" action="Admin_UpdateDistributor" method="post">
                        <p style="font-size: 25px; color: red">${Ok}</p>
                    <input hidden name="disid" value="${distributor.id}" type="text" readonly>
                    <div class="txt_field">
                        Tên Nhà Phân Phối<input name="disname" value="${distributor.name}" type="text" >
                    </div>
                    <div class="txt_field">
                        Số Điện Thoại<input name="disphone" value="${distributor.phone}" type="text" >

                    </div>
                    <div class="txt_field">
                        Địa Chỉ <input name="disaddress" value="${distributor.address}" type="text" >

                    </div>
                    <div class="signup_link">
                        Trang Thông Tin? <a href="Admin_Infor_Distributor.jsp">Trở về</a>
                    </div>

                    <input type="submit" value="UPDATE">

                </form>
            </div>
            <jsp:include page="Footer.jsp"></jsp:include>
        </c:if>
    </body>

</html>