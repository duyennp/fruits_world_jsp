<%-- 
    Document   : Cart
    Created on : Oct 24, 2021, 6:12:25 AM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop</title>
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/ghost.css">
    </head>

    <body>
        <!-- Thông báo lỗi khi người dùng sửa url -->
        <c:if test="${sessionScope.acc.isAdmin != 1}">
            <div class="notAdminError">
                <div class="container-ghost">
                    <a href="index.jsp">
                        <div class="ghost">
                            <div class="ghost-body">
                                <div class="ghost-eye"></div>
                                <div class="ghost-mouth"></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="notAdmin">
                    <h1 style="color: white" class="notAdmin-title">Bạn méo phải Admin đâu mà truy cập vào !!!!</h1>
                </div>
                <div class="dic">
                    <a class="notAdmin_link" href="index.jsp">Vâng ạ. Em thoát ra ngay :((</a>
                </div>
            </div>
        </c:if>
    </body>

</html>
