<%-- 
    Document   : Confirm
    Created on : Oct 24, 2021, 6:18:33 AM
    Author     : nhóm 5
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <jsp:useBean id="a" class="dao.FruitsDAO" scope="request"></jsp:useBean>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <!-- Mobile Metas -->
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <!-- Site Metas -->
            <title>Fruits Shop - Xác Nhận Đơn Hàng</title>
            <meta name="keywords" content="">
            <meta name="description" content="">
            <meta name="author" content="">

            <!-- Site Icons -->
            <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
            <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <!-- Site CSS -->
            <link rel="stylesheet" href="css/style.css">
            <!-- Responsive CSS -->
            <link rel="stylesheet" href="css/responsive.css">
            <!-- Custom CSS -->
            <link rel="stylesheet" href="css/custom.css">

        </head>

        <body>
            <!-- Danh sách các đơn order của khách hàng cần được xác nhận -->
        <jsp:include page="Error.jsp"></jsp:include>
        <c:if test="${sessionScope.acc.isAdmin == 1}">

            <jsp:include page="Header.jsp"></jsp:include>

                <!-- Start All Title Box -->
                <div class="all-title-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Đơn hàng chưa giao</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End All Title Box -->
            <jsp:include page="Admin_Option.jsp"></jsp:include>


                <!-- Start Wishlist  -->
            <c:set var="count" value="0" scope="request"></c:set>
            <c:if test="${a.listOrder.size()==0}">
                <h1 style="text-align: center; color: red;">Không còn khách hàng nào đang order</h1>
            </c:if>
            <c:forEach items="${a.listOrder}" var="listF">
                <div class="wishlist-box-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-main table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Tên tài khoản</th>
                                                <th>Sản phẩm</th>
                                                <th>Tên</th>
                                                <th>Số lượng ( KG )</th>
                                                <th>Giá ( VNĐ )</th>
                                                <th>Ngày Đặt Mua</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:set var="sum" value="0"></c:set>
                                            <c:forEach items="${listF}" var="fruit">
                                                <tr>
                                                    <td class="name-pr" style="font-weight: bold; color: black">
                                                        <a href="Admin_Infor_ViewCus?cname=${fruit.name}">
                                                            <p>${fruit.name}</p>
                                                        </a>
                                                    </td>
                                                    <td class="thumbnail-img">
                                                        <img class="img-fluid" src="${fruit.photo}" alt="Ảnh Sản Phẩm"/>
                                                    </td>
                                                    <td class="name-pr" style="font-weight: bold; color: black">    
                                                        ${fruit.namefruit}
                                                    </td>
                                                    <td class="price-pr" style="font-weight: bold; color: black">
                                                        <p style="font-weight: bold; color: black">${fruit.quantity}</p>
                                                    </td>
                                                    <td class="quantity-box" style="font-weight: bold; color: black">${fruit.price} </td>
                                                    <td class="name-pr">
                                                        <p style="font-weight: bold; color: black">${fruit.day}</p>
                                                    </td>
                                                </tr>
                                                <c:set var="sum" value="${sum+fruit.quantity*fruit.price}"></c:set>
                                            </c:forEach>
                                            <tr>
                                                <td class="add-pr">
                                                    <a class="btn hvr-hover" href="Admin_ConfirmOrder?stt=${count}">Xác Nhận Đã Giao Hàng</a>
                                                </td>
                                                <td class="add-pr">
                                                    <a class="btn hvr-hover" href="Admin_DisConfirmOrder?stt=${count}">Hủy Đặt Hàng</a>
                                                </td>
                                                <td class="price-pr">
                                                    <p style="color: black; font-size: larger; font-weight: bold">Tổng Tiền: ${sum}</p>
                                                </td>
                                            </tr>
                                            <c:set var="count" value="${count+1}" scope="request"></c:set>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </c:forEach>
            <!-- End Wishlist -->

            <jsp:include page="Footer.jsp"></jsp:include>
        </c:if>
    </body>

</html>