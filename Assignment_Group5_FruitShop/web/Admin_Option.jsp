<%-- 
    Document   : Admin_Option
    Created on : Oct 27, 2021, 7:47:51 PM
    Author     : nhóm 5
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Quản Lí</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">

    </head>
    <body>
        <!-- Nhửng lựa chọn năm trong khu quản lí của admin -->
        <div class="danhSachLuaChon add-pr" style="display: flex; justify-content: center; align-items: center; margin-top: 16px;">
            <a style="margin: 0 4px;" class="btn hvr-hover" href="Admin_Manager.jsp">Danh Sách Trái Cây</a>
            <a style="margin: 0 4px;" class="btn hvr-hover" href="Admin_Confirm.jsp">Danh Sách Order</a>
            <a style="margin: 0 4px;" class="btn hvr-hover" href="Admin_CreateFruits.jsp">Thêm Trái Cây</a>
            <a style="margin: 0 4px;" class="btn hvr-hover" href="Admin_Infor_Customer.jsp">Danh Sách KH</a>
            <a style="margin: 0 4px;" class="btn hvr-hover" href="Admin_Sell.jsp">Lịch Sử Bán Hàng</a>
        </div>
        <div class="danhSachLuaChon add-pr" style="display: flex; justify-content: center; align-items: center; margin-top: 16px; margin-bottom: 16px;">
            <a style="margin: 0 4px;" class="btn hvr-hover" href="Admin_AddFruitsFromDistributors.jsp">Nhập Hàng Từ Nhà Phân Phối</a>
            <a style="margin: 0 4px;" class="btn hvr-hover" href="Admin_Distributors.jsp">Thêm Nhà Phân Phối</a>
            <a style="margin: 0 4px;" class="btn hvr-hover" href="Admin_Infor_Distributor.jsp">Danh Sách NPP</a>
            <a style="margin: 0 4px;" class="btn hvr-hover" href="Admin_Buy.jsp">Lịch Sử Mua Hàng</a>
        </div>
    </body>
</html>
