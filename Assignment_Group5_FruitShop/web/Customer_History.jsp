<%-- 
    Document   : Customer_History
    Created on : Oct 24, 2021, 6:20:08 AM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Lịch Sử Mua Hàng</title>
        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">



    </head>

    <body>
        <!-- Hiển thị lịch sử mua hàng tại shop -->
        <jsp:include page="Header.jsp"></jsp:include>
        <jsp:include page="ErrorNullAccount.jsp"></jsp:include>
        <!-- Start All Title Box -->
         <c:if test="${sessionScope.acc !=null}">
        <div class="all-title-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Lịch Sử Mua Hàng</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- End All Title Box -->
       
        <c:if test="${list.size() > 0}">
        <!-- Start Cart  -->
        <div class="cart-box-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-main table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Sản Phẩm</th>
                                        <th>Tên</th>
                                        <th>Số Lượng ( KG )</th>
                                        <th>Giá ( VNĐ / KG )</th>
                                        <th>Ngày Mua</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${list}" var="x">
                                    <tr style="font-weight: bold">
                                        <td class="thumbnail-img">
                                                <img class="img-fluid" src="${x.image}" alt="" />
                                        </td>
                                        <td class="name-pr">
                                            <a href="#">
                                                ${x.name}
                                            </a>
                                        </td>
                                        <td >
                                            <p>${x.quantity}</p>
                                        </td>
                                        <td>
                                            <p>${x.price}</p>
                                        </td>
                                        <td>
                                            <p>${x.date}</p>
                                        </td>
                                    </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!-- End Cart -->
        </c:if>
        <c:if test="${list.size() <= 0}">
            <div class="my-account-box-main">
            <div class="container">
                <div class="my-account-page">
                    <div class="row">
                        <div style="margin-left: auto; margin-right: 230px" class="col-lg-4 col-md-12">
                                <div class="service-box">
                                    <div class="service-icon">
                                        <a  href="Page_Shop.jsp"><i class='fas fa-sad-cry' style='font-size:95px;color:yellow;margin-top: -3px; margin-left: -1px'></i> </a>
                                    </div>
                                    <div style="margin-left: -180px; margin-right: auto; width: 300%;">
                                        <h4 style=" font-weight: bold; font-size: 22px">Hiện Tại Qúy Khách Chưa Mua Hàng Tại Fruits Shop </h4>
                                    </div>
                                </div>

                        </div>                     
                    </div>
                </div>
            </div>
        </div>
        </c:if>
        </c:if>


        <jsp:include page="Footer.jsp"></jsp:include>
    </body>

</html>
