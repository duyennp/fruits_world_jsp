<%-- 
    Document   : Admin_CreateFruits
    Created on : Oct 25, 2021, 8:21:15 PM
    Author     : nhóm 5
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Tạo Trái Cây</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">
        <script src="js/myScript.js"></script>
    </head>

    <body>
        <!-- Form đăng kí -->
        <jsp:include page="Header.jsp"></jsp:include>


            <!-- Start All Title Box -->
            <div class="all-title-box">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Đăng kí</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End All Title Box -->

            <div class="center1">
                <h1 class="Login1">Đăng Kí</h1>
                <form action="Account_SignUp" method="post">
                    <p style="font-size: 25px; color: red">${Pass}</p>
                <p style="font-size: 25px; color: red">${User}</p>
                <div class="txt_field">
                    <input value="${username}" name="username" type="text" required>

                    <label>Tài Khoản</label>
                </div>
                <div class="txt_field">
                    <input value="${password}"  id="password" name="password" type="password" required>

                    <label>Mật Khảu</label>
                </div>
                <div class="txt_field">
                    <input value="${repassword}" id="repassword" name="repassword" type="password" required>
                    <label>Nhập Lại Mật Khảu</label>
                </div>
                <div> <input type="checkbox" onclick="showPass()">Hiện Mật Khẩu</div>
                <div class="txt_field">
                    <input  value="${yourname}" name="yourname" type="text" required>

                    <label>Họ Và Tên</label>
                </div>
                <div class="txt_field">
                    <input value="${birth}" name="birth" type="text" required>

                    <label>Năm Sinh</label>
                </div>
                <div class="txt_field">
                    <input value="${sdt}" name="sdt" type="text" required>

                    <label>Số Điện Thoại</label>
                </div>
                <div class="txt_field">
                    <input value="${address}" name="address" type="text" required>

                    <label>Địa Chỉ</label>
                </div>
                <div class="signup_link">
                    Bạn là thành viên? <a href="Page_Login.jsp">Đăng Nhập</a>
                </div>

                <input type="submit" value="Đăng Kí">

            </form>
        </div>
        <jsp:include page="Footer.jsp"></jsp:include>

    </body>

</html>

