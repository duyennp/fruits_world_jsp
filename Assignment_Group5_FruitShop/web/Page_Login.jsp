<%-- 
    Document   : Login
    Created on : Oct 24, 2021, 6:14:29 AM
    Author     : nhóm 5
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Đăng Nhập</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">
        <script src="js/myScript.js"></script>
    </head>

    <body>
        <!-- Form đăng nhập -->
        <jsp:include page="Header.jsp"></jsp:include>


            <!-- Start All Title Box -->
            <div class="all-title-box">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Đăng nhập</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End All Title Box -->

            <div class="center1">
                <h1 class="Login1">Đăng nhập</h1>
                <form action="Account_Login" method="post">
                    <p style="font-size: 25px; color: ${color}">${Ok}</p>
                <div class="txt_field">
                    <input value="${user}"name="username" type="text" required>
                    <label>Tên đăng nhập</label>
                </div>
                <div class="txt_field">
                    <input id="myPass" name="password" type="password" required>
                    <label>Mật khẩu</label>
                </div>
                <div> <input type="checkbox" onclick="myFunction()">Hiện Mật Khẩu</div>
                <input type="submit" value="Login">
                <div class="signup_link">
                    Chưa phải thành viên? <a href="Page_SignUp.jsp">Đăng ký</a>
                </div>
            </form>
        </div>

        <jsp:include page="Footer.jsp"></jsp:include>
    </body>

</html>