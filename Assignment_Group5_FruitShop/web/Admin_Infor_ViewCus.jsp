<%-- 
    Document   : Admin_Infor_ViewCus
    Created on : Nov 5, 2021, 8:47:52 PM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Thông Tin Khách Hàng</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">

    </head>

    <body>
        <!--check admin-->
        <jsp:include page="Error.jsp"></jsp:include>
        <c:if test="${sessionScope.acc.isAdmin ==1}">
        <jsp:include page="Header.jsp"></jsp:include>
            <!-- Start All Title Box -->
            <div class="all-title-box">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Thông Tin</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End All Title Box -->

            <!--hiển thị thông tin khách hàng từ danh sach order-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-main table-responsive">
                        <table style="width: 50%; margin-right: auto; margin-left: auto" class="table">
                            <thead>
                                <tr>
                                    <th colspan="2" style="text-align: center">Thông Tin Khách Hàng</th>                                        
                                </tr>
                            </thead>
                            <tbody style="font-weight: bold">
                                <tr>
                                    <td>
                                        Tài khoản
                                    </td>
                                    <td>
                                        ${cinfor.username}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ten Khách Hàng</td>
                                    <td>${cinfor.yourname}</td>
                                </tr>
                                <tr>
                                    <td>Năm Sinh</td>
                                    <td>${cinfor.birth}</td>
                                </tr>
                                <tr>
                                    <td>SĐT</td>
                                    <td>${cinfor.sdt}</td>
                                </tr>
                                <tr>
                                    <td>Đia Chỉ</td>
                                    <td>${cinfor.address}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;"><a style="color: red" href="Admin_Confirm.jsp">Quay lại</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </c:if>
        <jsp:include page="Footer.jsp"></jsp:include>
    </body>
</html>
