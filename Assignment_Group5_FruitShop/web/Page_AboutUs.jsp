<%-- 
    Document   : AboutUs
    Created on : Oct 24, 2021, 6:06:15 AM
    Author     : nhóm 5
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Thông Tin Shop</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">
    </head>

    <body>
        <!-- Giới thiệu về shop -->
        <jsp:include page="Header.jsp"></jsp:include>
            <!-- Start All Title Box -->
            <div class="all-title-box">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Chúng Tôi là Fruits Shop</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End All Title Box -->

            <!-- Start About Page  -->
            <div class="about-box-main">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="banner-frame"> <img class="img-fluid" src="images/shop.jpg" alt="Ảnh Cửa Hàng" />
                            </div>
                        </div>
                        <div style="font-weight: bold" class="col-lg-6">
                            <h2 class="noo-sh-title-top"> <span>CHÚNG TÔI LÀ FRUITS SHOP</span></h2>
                            <p>Fruits shop là một web chuyên bán sỉ các loại trái. Web của chúng tôi đa dạng mặt hàng từ nội địa đến các loại nhập khẩu đều có. Shop của chúng tôi có chất lương trái cây hàng đầu, các loiaj trái cây đều đảm 
                                bảo nguồn gốc,  xuất xứ và không có chất bảo quản. Vì chúng tôi có kho hàng lớn có các thiết bị công nghệ cao bảo quản. Sản phẩm thì đều không có thuốc trừ sâu nên đảm bảo sức khỏe cho người tiêu 
                                dùng và đồng thời các sản phẩm đều mạng các chất dinh dưỡng cao. Web của chúng tôi có đầy đủ chức năng để người dùng tham khảo sản phẩm và đồng thời shop chngs tôi luôn cập nhật các thông tin mới 
                                nhất về các loại trái cây. Web của chúng tôi luôn được bảo mật cao về thông tin người dùng vì Web của chúng tôi có đội ngũ IT chuyên nghiệp. Tiếp theo là phần giao hàng thì nhanh chống trong 24h và mọi ch 
                                phí vận chuyển đều được miễn phí. </p>
                            <p>Tóm lại Fruits Shop là một shop bán hàng uy tín bật nhất Việt Nam. Nên vì thế mọi người hãy đến với shop của chúng tôi</p>
                        </div>
                    </div>
                    <div class="row my-5">
                        <div class="col-sm-6 col-lg-4">
                            <div class="service-block-inner">
                                <h3>CHÚNG TÔI TIN TƯỞNG</h3>
                                <p>Shop của chúng tôi là một shop bán trái cây uy tín và chất lượng ở Việt Nam. Khó có một shop nào có sự chuyên nghiệp hơn shop của chúng tôi. </p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="service-block-inner">
                                <h3>CHÚNG TÔI CHUYÊN NGHIỆP</h3>
                                <p>Về vấn đề này thì shop có đội ngũ nhân viên,  quản lí, IT vô cùng chuyên nghiệp được đào tạo bài bản và kinh nghiệm dồi giàu có thể phục vụ tốt nhất cho khách hàng. </p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-4">
                            <div class="service-block-inner">
                                <h3>CHÚNG TÔI CÓ CHẤT LƯỢNG</h3>
                                <p>Fruits shop đều nhập mặt hàng chất lương vì nhập hàng từ các vườn đạt chuẩn vietgap và mỗi sản phẩm đều có giá trị dinh dưỡng cao. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <jsp:include page="Footer.jsp"></jsp:include>

    </body>

</html>
