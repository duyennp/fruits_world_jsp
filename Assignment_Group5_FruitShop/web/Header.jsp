<%-- 
    Document   : Header
    Created on : Oct 28, 2021, 8:16:23 AM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Start Main Top -->
<html>
    <body>
        <div class="main-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <c:if test="${sessionScope.acc != null}">
                            <div class="our-link">
                                <ul>
                                    <li><a href="Page_MyAccount.jsp"><i class="fa fa-user s_color"></i> <p> ${sessionScope.acc.username}<p></p></a></li>
                                </ul>
                            </div>
                        </c:if>
                        <c:if test="${sessionScope.acc == null}">
                            <div class="our-link">
                                <ul>
                                    <li><a href="Page_MyAccount.jsp"><i class="fa fa-user s_color"></i> Đăng Nhập</a></li>
                                </ul>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Main Top -->

        <!-- Start Main Top -->
        <header class="main-header">
            <!-- Start Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
                <div class="container">
                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="index.jsp"><img style="margin-top: -11px" src="images/logo2.jpg" class="logo" alt="logo"></a>
                    </div>
                    <!-- End Header Navigation -->

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                            <li class="nav-item "><a class="nav-link" href="index.jsp">Trang Chủ</a></li>
                            <li class="nav-item"><a class="nav-link" href="Page_AboutUs.jsp">Giới Thiệu</a></li>
                            <li class="nav-item"><a class="nav-link" href="Page_Shop.jsp">Trái Cây</a></li>
                            <li class="nav-item"><a class="nav-link" href="Page_Contact.jsp">Liên Hệ</a></li>
                                <c:if test="${sessionScope.acc == null}">                       
                                <li class="nav-item"><a class="nav-link" href="Account_Null">Giỏ Hàng</a></li>
                                </c:if>
                                <c:if test="${sessionScope.acc != null }">                       
                                <li class="nav-item"><a class="nav-link" href="Cart_Show">Giỏ Hàng</a></li>
                                </c:if>
                                <c:if test="${sessionScope.acc.isAdmin == 1 }">                        
                                <li class="nav-item"><a class="nav-link" href="Admin_Manager.jsp">Quản lí Kho hàng của Admin</a></li>
                                </c:if>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navigation -->
        </header>
        <!-- End Main Top -->
    </body>
</html>