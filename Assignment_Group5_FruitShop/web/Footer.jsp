<%-- 
    Document   : Footer
    Created on : Oct 27, 2021, 7:20:28 PM
    Author     : nhóm 5
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">

    </head>
    <body>

        <!-- Start Footer  -->
        <footer class="footer-f">
            <div class="footer-main">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <div class="footer-top-box">
                                <h3>Thời gian hoạt động</h3>
                                <ul class="list-time">
                                    <li>Thứ 2 - Thứ 6: 08.00am to 05.00pm</li> <li>Thứ 7: 10.00am to 08.00pm</li> <li>Chủ nhật: <span>Đóng cửa</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <div class="footer-top-box">
                                <h3>Bản tin</h3>
                                <form class="newsletter-box">
                                    <div class="form-group">
                                        <input class="" type="email" name="Email" placeholder="Địa chỉ Email*" />
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                    <button class="btn hvr-hover" type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <div class="footer-top-box">
                                <h3>Phương tiện truyền thông</h3>
                                <p>Phục vụ khách hàng là niềm vui của chúng tôi.</p>
                                <ul>
                                    <li><a href="https://www.facebook.com/NguyenPhuocDuyen79"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                                    <!--<li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>-->
                                    <li><a href="https://www.linkedin.com/in/duyen-nguyen-b369a221b/"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                                    <!--<li><a href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a></li>-->
                                    <!--<li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>-->
                                    <!--<li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>-->
                                    <!--<li><a href="#"><i class="fab fa-whatsapp" aria-hidden="true"></i></a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">


                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <div class="footer-link-contact">
                                <h4>Liên hệ</h4>
                                <ul>
                                    <li>
                                        <p><i class="fas fa-map-marker-alt"></i>Địa chỉ: Đại Học FPT Cần Thơ</p>
                                    </li>
                                    <li>
                                        <p><i class="fas fa-phone-square"></i>Điện thoại: <a href="tel:0788751149">0788751149</a></p>
                                    </li>
                                    <li>
                                        <p><i class="fas fa-envelope"></i>Email: <a href="mailto:duyennpce150850@fpt.edu.vn"><abbr>duyennpce150850@fpt.edu.vn</abbr></a></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer  -->
    </body>
</html>
