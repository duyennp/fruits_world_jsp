<%-- 
    Document   : MyAccount
    Created on : Oct 24, 2021, 6:24:46 AM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Account</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">

    </head>

    <body>
        <!-- Hiển thị một số chức năng của tài khoản -->
      <jsp:include page="Header.jsp"/>


        <!-- Start All Title Box -->
        <div class="all-title-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Tài Khoản</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- End All Title Box -->


       <!-- Start My Account  -->
        <div class="my-account-box-main">
            <div class="container">
                <div class="my-account-page">
                    <div class="row">
                        <c:if test="${sessionScope.acc != null}">
                        <div class="col-lg-4 col-md-12">
                            <div class="account-box">
                                <div class="service-box">
                                    <div class="service-icon">
                                        <a href="Customer_History"> <i class="fa fa-gift"></i> </a>
                                    </div>
                                    <div class="service-desc">
                                        <h4>Lịch Sử Mua Hàng</h4>
                                        <p>Trãi nghiệm của Bạn tại Fruits Shop</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="account-box">
                                <div class="service-box">
                                    <div class="service-icon">
                                        <a href="Account_LogOut"><i class="fa fa-lock"></i> </a>
                                    </div>
                                    <div class="service-desc">
                                        <h4>Đăng Xuất</h4>
                                        <p>Tạm Biệt Qúy Khách</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="account-box">
                                <div class="service-box">
                                    <div class="service-icon">
                                        <a href="Customer_Information.jsp"> <i class="fa fa-location-arrow"></i> </a>
                                    </div>
                                    <div class="service-desc">
                                        <h4>Thông Tin Tài Khoản</h4>
                                        <p>Xem và chỉnh sửa thông tin của Bạn</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </c:if>
                        <c:if test="${sessionScope.acc == null}">
                            <div style="margin-right: auto; margin-left: auto" class="col-lg-4 col-md-12">
                            <div class="account-box">
                                <div class="service-box">
                                    <div class="service-icon">
                                        <a href="Page_Login.jsp"><i class="fa fa-lock"></i> </a>
                                    </div>
                                    <div class="service-desc">
                                        <h4>Đăng Nhập / Đăng Ký</h4>
                                        <p>Để mua hàng Bạn cần có tài khoản</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    <!-- End My Account -->
   <jsp:include page="Footer.jsp"/> 
</body>

</html>
