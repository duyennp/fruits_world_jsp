<%-- 
    Document   : Admin_AddFruitsList
    Created on : Oct 26, 2021, 10:10:32 AM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Xác Nhận Đơn Hàng</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/responsive.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">

    </head>

    <body>
        <!-- Nhập Tên Và số lượng trái cây từ nhà phân phối-->
        <jsp:include page="Error.jsp"></jsp:include>
        <c:if test="${sessionScope.acc.isAdmin == 1}">
            <jsp:include page="Header.jsp"></jsp:include>


                <!-- Start All Title Box -->
                <div class="all-title-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Xác nhận nhập hàng</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End All Title Box -->

                <!-- Start Wishlist  -->
                <div class="wishlist-box-main">
                    <div class="inforNhaPhanPhoi">
                        <p class="tenNhaPhanPhoi" style="color: black;font-weight: bold">Nơi Nhập Hàng: ${distributors.name}</p>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-main table-responsive">
                                <form action="Admin_BuyfromDistributor" method="get">
                                    <table class="table" >
                                        <thead>
                                            <tr>
                                                <th>Tên</th>
                                                <th>Số Lượng ( KG )</th>
                                                <th>Giá ( VNĐ )</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            <c:forEach begin="1" end="${quantityFruit}" var="z">
                                                <tr>
                                                    <td class="name-pr">
                                                        <select id="nameFruits${z}" name="nameFruits${z}" style="width: 100%;height: 36px;">
                                                            <c:forEach items="${listF}" var="x">
                                                                <option value="${x.id}">${x.name}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                    <td class="quantity-box">
                                                        <input type="number"  id="quantity${z}" name="quantity${z}" min="1" placeholder="Nhập Số Lượng Trái Cây">
                                                    </td>
                                                    <td class="quantity-box">
                                                        <input type="number"  id="priceFruit${z}" name="priceFruit${z}" min="1" placeholder="Nhập Giá Trái Cây">
                                                    </td>
                                                </tr>
                                            </c:forEach>

                                            <tr>
                                                <td class="add-pr"colspan="3" style="align-items: center; align-content: center;">
                                                    <input class="btn hvr-hover" type="submit" value="XacNhan" >
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Wishlist -->

            <jsp:include page="Footer.jsp"></jsp:include>
        </c:if>
    </body>

</html>