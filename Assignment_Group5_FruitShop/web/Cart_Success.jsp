<%-- 
    Document   : Cart
    Created on : Oct 24, 2021, 6:12:25 AM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Giỏ Hàng</title>
        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">



    </head>

    <body>
        <!-- Hiển thị sau khi người dùng thanh toán giỏ hàng thành công -->
        <jsp:include page="Header.jsp"></jsp:include>
        <!-- Start All Title Box -->
            <div class="all-title-box">
                <div class="container">
                    <div class="row">
                    </div>
                </div>
            </div>
            <!-- End All Title Box -->
            <div class="my-account-box-main">
            <div class="container">
                <div class="my-account-page">
                    <div class="row">
                        <div style="margin-left: auto; margin-right: 230px" class="col-lg-4 col-md-12">
                                <div class="service-box">
                                    <div class="service-icon">
                                        <a  href="Page_Shop.jsp"><i class='fas fa-smile-beam' style='font-size:97px;color:yellow;margin-top: -4px; margin-left: -2px'></i> </a>
                                    </div>
                                    <div style="margin-left: -455px; margin-right: auto; width: 300%;">
                                        <h4 style=" font-weight: bold; font-size: 22px; color: green">Đơn Hàng Của Qúy Khách Đã Được Gửi Đến ADMIN, Qúy Khách Vui Lòng Chờ Hóa Đơn Được Xác Nhận. Cảm Ơn Qúy Khách</h4>
                                    </div>
                                    
                                    <div style="margin-left: -50px">
                                        <button class="btn hvr-hover"><a style="font-weight: bold; font-size: 22px" href="Page_Shop.jsp">Tiếp Tục Mua Hàng</a></button>
                                    </div>
                                </div>

                        </div>                     
                    </div>
                </div>
            </div>
        </div>

        <jsp:include page="Footer.jsp"></jsp:include>
    </body>

</html>
