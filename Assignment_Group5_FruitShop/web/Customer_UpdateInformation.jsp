<%-- 
    Document   : Update_Information
    Created on : Oct 24, 2021, 6:22:27 AM
    Author     : nhóm 5
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Chỉnh Sử thông tin Khách Hàng</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">
        <script src="js/myScript.js"></script>
    </head>

    <body>
        <!-- Chỉnh sửa thông tin tài khoản -->
        <jsp:include page="Header.jsp"></jsp:include>
        <jsp:include page="ErrorNullAccount.jsp"></jsp:include>
        <c:if test="${sessionScope.acc !=null}">
            <!-- Start All Title Box -->
            <div class="all-title-box">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Chỉnh Sửa Thông Tin</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End All Title Box -->

            <div class="center1">
                <h1 >Thay Đổi</h1>
                <form class="form" action="Customer_UpdateInformation" method="get">
                    <p style="font-size: 25px; color: red">${Ok}</p>
                <div class="txt_field">
                    Tài Khoản  <input name="username" value="${sessionScope.acc.username}" type="text" readonly>  
                </div>
                <div class="txt_field">
                    Tên <input name="yourname" value="${sessionScope.acc.yourname}" type="text" >
                </div>
                <div class="txt_field">
                    Năm Sinh <input name="birth" value="${sessionScope.acc.birth}" type="text" >

                </div>
                <div class="txt_field">
                    Số Điện Thoại<input name="sdt" value="${sessionScope.acc.sdt}" type="text" >

                </div>
                <div class="txt_field">
                    Địa Chỉ <input name="address" value="${sessionScope.acc.address}" type="text" >

                </div>
                <div class="signup_link">
                    Trang Thông Tin? <a href="Customer_Information.jsp">Trở về</a>
                </div>

                <input type="submit" value="UPDATE">

            </form>
        </div>
                    </c:if>
        <jsp:include page="Footer.jsp"></jsp:include>
    </body>

</html>
