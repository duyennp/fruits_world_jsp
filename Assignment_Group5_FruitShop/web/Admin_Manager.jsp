<%-- 
    Document   : Manager
    Created on : Oct 24, 2021, 6:13:17 AM
    Author     : nhóm 5
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <jsp:useBean id="a" class="dao.FruitsDAO" scope="request"></jsp:useBean>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">

            <!-- Mobile Metas -->
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <!-- Site Metas -->
            <title>Fruits Shop - Quản Lí</title>
            <meta name="keywords" content="">
            <meta name="description" content="">
            <meta name="author" content="">

            <!-- Site Icons -->
            <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
            <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <!-- Site CSS -->
            <link rel="stylesheet" href="css/style.css">
            <!-- Responsive CSS -->
            <link rel="stylesheet" href="css/responsive.css">
            <!-- Custom CSS -->
            <link rel="stylesheet" href="css/custom.css">

        </head>

        <body>
            <!-- Khu vực quản lí của admin -->
        <jsp:include page="Error.jsp"></jsp:include>
        <c:if test="${sessionScope.acc.isAdmin == 1}">
            <jsp:include page="Header.jsp"></jsp:include>


                <!-- Start All Title Box -->
                <div class="all-title-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Quản Lí</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End All Title Box -->
            <jsp:include page="Admin_Option.jsp"></jsp:include>
                <!-- Start Wishlist  -->
                <div class="wishlist-box-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-main table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Sản phẩm</th>
                                                <th>Tên</th>
                                                <th>Số Lượng ( KG )</th>
                                                <th>Giá ( VNĐ )</th>
                                                <th>Hành Động</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <c:forEach items="${a.inforAllFruits}" var="j">
                                            <tr>
                                                <td class="thumbnail-img">
                                                    <img class="img-fluid" src="${j.photo}" alt="Ảnh Sản Phẩm" />
                                                </td>
                                                <td class="name-pr" style="font-weight: bold; color: black">
                                                    ${j.name}
                                                </td>
                                                <td class="price-pr">
                                                    <p style="font-weight: bold; color: black">${j.quantity}</p>
                                                </td>
                                                <td class="quantity-box"  style="font-weight: bold; color: black">${j.price} VNĐ</td>
                                                <td class="add-pr" >
                                                    <div style="display: flex;justify-content: space-around">
                                                        <a class="btn hvr-hover" href="Admin_EditFruit?fid=${j.id}"> Chỉnh Sửa </a>
                                                        <a class="btn hvr-hover" href="Admin_DeleteFruit?fid=${j.id}"> Xóa trái cây </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Wishlist -->

            <jsp:include page="Footer.jsp"></jsp:include>
        </c:if>
    </body>

</html>
