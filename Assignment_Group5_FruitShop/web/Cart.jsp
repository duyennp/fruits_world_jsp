<%-- 
    Document   : Cart
    Created on : Oct 24, 2021, 6:12:25 AM
    Author     : nhóm 5
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!-- Basic -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Site Metas -->
        <title>Fruits Shop - Giỏ Hàng</title>
        <!-- Site Icons -->
        <link rel="shortcut icon" href="images/Icon.jpg" type="image/x-icon">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">



    </head>

    <body>
        <!-- Hiện thị giỏ hàng của từng người dùng -->
        <jsp:include page="Header.jsp"></jsp:include>
        <jsp:include page="ErrorNullAccount.jsp"></jsp:include>
        <c:if test="${sessionScope.acc !=null}">
            <!-- Start All Title Box -->
            <div class="all-title-box">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Giỏ Hàng</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End All Title Box -->
        <c:if test="${list.size() > 0}">
            <!-- Start Cart  -->
            <div class="cart-box-main">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-main table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Sản Phẩm</th>
                                            <th>Tên</th>
                                            <th>Số Lượng ( KG )</th>
                                            <th>Giá ( VNĐ )</th>
                                            <th>Loại Bỏ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${list}" var="x">
                                            <tr style="font-weight: bold">
                                                <td class="thumbnail-img">
                                                    <a href="Fruit_Choose?fid=${x.id}">
                                                        <img class="img-fluid" src="${x.image}" alt="" />
                                                    </a>
                                                </td>
                                                <td class="name-pr">
                                                    <p>
                                                        ${x.name}
                                                    </p>
                                                </td>
                                                <td >
                                                    <p>${x.quantity}</p>
                                                </td>
                                                <td>
                                                    <p>${x.price}</p>
                                                </td>
                                                <td>
                                                    <a href="#" onclick="showMess(${x.id})">
                                                        X           
                                                    </a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row my-5">
                        <div class="col-lg-6 col-sm-6">
                            <div class="coupon-box">
                                <form action="Cart_Show" method="post">
                                    <div class="input-group input-group-sm">

                                        <input name="code" class="form-control" placeholder="Hãy Nhập Mã Giảm Giá" type="text">
                                        <div class="input-group-append">
                                            <input class="btn btn-theme" type="submit" value="Áp dụng mã giãm giá">
                                        </div>
                                        
                                    </div>
                                    <div>
                                        <h4 style="color: red; margin-top: 10px">${Mess}</h4>
                                        </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-12"></div>
                        <div class="col-lg-4 col-sm-12">
                            <div class="order-box">
                                <h3>Tổng Giá Của Giỏ Hàng</h3>
                                <div class="d-flex">
                                    <h4>Tổng</h4>
                                    <div class="ml-auto font-weight-bold"> ${sum} VNĐ </div>
                                </div>
                                <div class="d-flex">
                                    <h4>Giảm</h4>
                                    <div class="ml-auto font-weight-bold"> - 0 VNĐ </div>
                                </div>
                                <hr class="my-1">
                                <div class="d-flex">
                                    <h4>Mã Giảm Giá</h4>
                                    <div class="ml-auto font-weight-bold"> - ${sale} VNĐ </div>
                                </div>
                                <div class="d-flex">
                                    <h4>Phí Vận Chuyển</h4>
                                    <div class="ml-auto font-weight-bold"> Miễn Phí </div>
                                </div>
                                <hr>
                                <div class="d-flex gr-total">
                                    <h5>Tổng cộng</h5>
                                    <div class="ml-auto h5"> ${sum - sale} VNĐ </div>
                                </div>
                                <hr> </div>
                        </div>
                        <div class="col-12 d-flex shopping-box"><a href="Cart_Purchase" class="ml-auto btn hvr-hover">Đặt hàng</a> </div>
                    </div>

                </div>
            </div>
            <!-- End Cart -->
        </c:if>
        <c:if test="${list.size() <= 0}">
            <div class="my-account-box-main">
            <div class="container">
                <div class="my-account-page">
                    <div class="row">
                        <div style="margin-left: auto; margin-right: 230px" class="col-lg-4 col-md-12">
                                <div class="service-box">
                                    <div class="service-icon">
                                        <a  href="Page_Shop.jsp"><i class='fas fa-sad-cry' style='font-size:95px;color:yellow;margin-top: -3px; margin-left: -1px'></i> </a>
                                    </div>
                                    <div style="margin-left: -180px; margin-right: auto; width: 300%;">
                                        <h4 style=" font-weight: bold; font-size: 22px">Hiện Tại Qúy Khách Chưa Có Sản Phẩm Trong Giỏ Hàng </h4>
                                    </div>
                                </div>

                        </div>                     
                    </div>
                </div>
            </div>
        </div>
        </c:if>
            </c:if>
        <script>
            function showMess(id) {
                var option = confirm("Bạn có chắc muốn xóa Sản Phẩm này?");
                if (option === true) {
                    window.location.href = 'Cart_Delete?sid=' + id;
                }
            }
        </script>
        <jsp:include page="Footer.jsp"></jsp:include>
    </body>

</html>
